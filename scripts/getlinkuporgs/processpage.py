#!/usr/bin/env python

import pathlib
import argparse
import re

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
            'orgpagepath',
            type=pathlib.Path
        )
    parser.add_argument(
            '--dry-run',
            default=False,
            action='store_true'
        )
    return parser.parse_args()

def get_page_content(pagepath):
    return pagepath.read_text()

def clean_common_tags(content):
    cleaners = [
        ('<img.*?>', ' '),
        ('.200b.', ''),
        ('<font size="\d">', ''),
        ('<font.*?>', ''),
        ('</font>', ''),
        ('<b>', ' *'),
        ('</b>', '* '),
        ('<wb>', ''),
        ('<br>', ''),
        ('&nbsp;', ''),
        ('<.?em>', ' '),
        ('<.?p>', ' '),
        ('<p .*?>', ' '),
        ('<.?ul>', ''),
        ('<li>', '- '),
        ('</li>', ''),
        ('&amp;', '&'),
        ('<div.*?>', ' '),
        ('</div>', ' '),
        ('<a href="(.*?)">(.*?)</a>', r'[\2](\1)'),
        ('<a .*?</a>', ''),
        ('<strong>', r' *'),
        ('</strong>', r'* '),
        ('<h1.*?>(.*?)</h1>', r'# \1'),
        ('<h2.*?>(.*?)</h2>', r'## \1'),
        ('<h3.*?>(.*?)</h3>', r'### \1'),
        ('<h4.*?>(.*?)</h4>', r'#### \1'),
        ('<h5.*?>(.*?)</h5>', r'##### \1'),
        ('<.?span>', ' '),
        ('<span .*?>', ' '),
        ('&#039;', "'"),
        ('\*>', '*'),
        ('</a>', ''),
        ('</?i>', ' '),
        ('\*{2,}', '*'),
        (' {2,}', ''),
        ('^\s\+', ''),
    ]
    for pat, rep in cleaners:
        #print('----')
        content = re.sub(pat, rep, content)
        #print(pat)
        #print(content)
        #print('----')

    content = '\n'.join([x for x in content.splitlines() if x])
    return content

def write_contents(contents, pagepath):
    print(f'saving {pagepath}')
    pagepath.write_text(contents)

def main():
    args = parse_args()
    contents = get_page_content(args.orgpagepath)
    contents = clean_common_tags(contents)
    if args.dry_run:
        print(contents)
    else:
        write_contents(contents, args.orgpagepath)

if __name__ == '__main__':
    main()
