#!/bin/sh

orgpages_path=content/page/orgpages

sed -i -e 's/- nationwide/- alabama\n- alaska\n- arizona\n- arkansas\n- california\n- colorado\n- connecticut\n- delaware\n- florida\n- georgia\n- hawaii\n- idaho\n- illinois\n- indiana\n- iowa\n- kansas\n- kentucky\n- louisiana\n- maine\n- maryland\n- massachusetts\n- michigan\n- minnesota\n- mississippi\n- missouri\n- montana\n- nebraska\n- nevada\n- new hampshire\n- new jersey\n- new mexico\n- new york\n- north carolina\n- north dakota\n- ohio\n- oklahoma\n- oregon\n- pennsylvania\n- rhode island\n- south carolina\n- south dakota\n- tennessee\n- texas\n- utah\n- vermont\n- virginia\n- washington\n- west virginia\n- wisconsin\n- wyoming/' ${orgpages_path}/*
