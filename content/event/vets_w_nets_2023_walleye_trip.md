---
title: Vets with Nets annual Walleye trip
endDate: 2023-06-10
startDate: 2023-06-09
---
Detroit Beach Boat Club 3028 Harborview Street Monroe, MI 48162

Vets With Nets mission is to show appreciation for our active military service members and veterans by providing world class walleye fishing adventures on the western basin of lake Erie. The intention is to leave every veteran or active service member who participates in our events with a memorable experience that gives them a renewed sense of pride in their service, provides hope for their future and a general positive outlook for the community in which they live and serve.

[Event Link](https://www.eventbrite.com/e/vets-with-nets-2023-tickets-511838411467?fbclid=IwAR1Vk4f1bCFU1CgbSx-DYyJtdWVacka3HmMartuC7jSuYlTtlLVP1-7DcY4)
