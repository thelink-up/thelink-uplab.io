---
title: National Disabled Veterans Winter Sports Clinic
enddate: 2023-03-31
startdate: 2023-03-25
---
The National Disabled Veterans Winter Sports Clinic is headed to the valley soon, and the Roaring Fork Valley Fly Fishing Club is hosting the fishing venue again this year between March 25-31

Where: Valleywide. Fishing venue is at the Half-As Ranch, 1087 Hooks Spur Lane in Basalt.
When: March 25-31, 9:30 a.m.-3 p.m., each day.
Contact: Tom Skutely, resident of the Roaring Fork Valley Fly Fishing Club: rfvffc@gmail.com
