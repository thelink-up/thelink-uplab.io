---
title: Camp Grafton Disabled Veterans Deer Hunt
enddate: 2023-11-20
startdate: 2023-03-21
---
Veterans Service Offices across the state in cooperation with the North Dakota Department of Game & Fish would like to announce the 2023 Camp Grafton Disabled Veteran Deer Hunt for Veterans who are 50% disabled or greater.

The Disabled Veteran Deer Hunt will take place Monday, November 13th and Monday, November 20th at Camp Grafton, North Dakota. Only 5 hunters allowed per day. Applications should be postmarked by April 29, 2023 to be considered.

Applications are available from your local County Veterans Service Office. PLEASE NOTE: This is a shotgun with slug only hunt, no rifles will be allowed or provided.
