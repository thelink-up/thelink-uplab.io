---
title: Marysville Sporstman's Association Veteran's Fishing Derby
endDate: 2023-06-04
startDate: 2023-06-03
---
https://marysvillesportsmen.org

On June 4, the association will host the fifth Veterans’ Derby and is planning to make it a special event to celebrate. They plan to have live music, lunch and a free hat and T-shirt for every veteran attending. They also will award the winner of the derby with a fishing trip to Lake Ontario.
