---
title: Heroes New Hope Foundation
countries:
- usa
statesprovinces:
- indiana
categories:
- hunting
- fishing
tags:
- deer
- turkey
- fishing
orgURL: https://www.heroesnewhope.org
---
Heroes New Hope Foundation, Inc. is a 501©(3) organization formed in May of 2016 by Scott Goodman after a successful joint venture with Blind Endeavors - another 501©(3) that serves combat-wounded veterans – that came in the form of a turkey hunt for blinded veterans from across the United States.  
​
After seeing firsthand, the needs of our veterans and their families who have sacrificed so much of themselves for our freedom, it is now time for us to give back to them.
​
Heroes New Hope Foundation provides all-inclusive ecotherapy opportunities included but not limited to an annual spring turkey hunt, summer fishing trip, fall deer hunt and other outdoor activities.
