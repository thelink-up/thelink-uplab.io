---
title: United Special Sportsman's Alliance
countries:
- usa
statesprovinces:
- wisconsin
categories:
- hunting
- fishing
tags:
orgURL: https://childswish.org
---
# About United Special Sportsman Alliance
 
United Special Sportsman Alliance, Inc. (USSA) is a 501(c) (3) non-profit “dream wish” granting charity, that specializes in sending critically-ill and disabled youth and disabled veterans on the outdoor adventure of their dreams! USSA adventures give our youth and veterans something to look forward to, and help sustain them in their time of need! Families are whisked away from the mundane, man-made world of hospitals, and high medical bills by giving them a place of peace to focus on the quality of life, family ties, and the wonders of our natural world! USSA is composed of a 100% volunteer staff from all walks of life, bonded together by a common love for our fellow mankind and a deep respect and appreciation of our world’s natural resources. By working cooperatively with caring “Adventure” donors as well as generous individual and corporate sponsors, USSA has made a significant impact on the lives of thousands of children, disabled veterans and their families. The public’s image of all sportsmen is enhanced, through this valuable community service and new lifelong friendships are made! To learn more about USSA read on.


