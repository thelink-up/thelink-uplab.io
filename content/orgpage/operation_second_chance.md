---
title: Operation Second Chance
countries:
- usa
statesprovinces:
- maryland
categories:
- retreat
orgURL: https://operationsecondchance.org
---
Operation Second Chance, Inc., a 501 (c) 3 organization, supports wounded servicemen and women recovering at Walter Reed National Military Medical Center (Bethesda, MD) and assists them as they transition back to duty or civilian life.
Mission
We are patriotic citizens committed to serving our wounded, injured and ill combat veterans. We support Veterans and their families while they recover in military hospitals, by building relationships and identifying and supporting immediate needs and interests. We are dedicated to promoting public awareness of the many sacrifices made by our Armed Forces.
