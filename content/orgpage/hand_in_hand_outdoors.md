---
title: American Heroes Project (Hand in Hand Outdoors)
countries:
- usa
statesprovinces:
- Utah
categories:
- hunting
- fishing
tags:
orgURL: https://americanheroesproject.org
---
Hand in Hand Outdoors mission statement is to provide opportunities for our nations veterans as well as underprivileged and disabled children and adults to experience outdoor activities to enhance their lives through education, experience and direct participation in fishing, boating, shooting and camping.
