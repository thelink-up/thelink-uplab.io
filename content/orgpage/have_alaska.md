---
title: Have Alaska
countries:
- usa
statesprovinces:
- alaska
categories:
- fishing
- outdoors
- hunting
tags:
- boating
- whitewater rafting
- hunting
- kayaking
- flight seeing
- snowmobile tours
- gold panning
- bicycling
- cruise vacations
- cultural tours
- wildlife viewing
- National Park tours
- dog mushing
- skiing
orgURL: http://www.havealaska.org/
---
HAVE-Alaska is an Alaska-based organization dedicated to promoting the physical and psychological rehabilitation of American Veterans injured in service to our country through outdoor activities and travel.  Our mission is to fund and facilitate activities in Alaska for injured American Veterans, their spouses, and dependents.  We provide activities such as fishing, boating, whitewater rafting, hunting, kayaking, flight seeing, snowmobile tours, gold panning, bicycling, cruise vacations, cultural tours, wildlife viewing, National Park tours, dog mushing, skiing, etc.     

We strive to raise awareness and enlist the public’s aid for the needs of injured service members and their families. We hope to educate people concerning their role in the rehabilitation and societal acceptance of disabled service members.

We provide other organizations logistical assistance and programs to provide these once-in-a-lifetime experiences. We support all fellow veterans organizations that will directly and/or indirectly benefit American service members, and their families.  Our members have worked extensively with the Wounded Warrior Project, Wounded Warrior Wives, Am-Vets, Project Healing Waters, Paralyzed Veterans of America, The Armed Services YMCA, and others to provide World-class experiences for our most deserving citizens.

Please help us give back to those willing to give all for your freedom!
