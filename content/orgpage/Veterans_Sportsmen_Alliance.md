---
title: Veterans Sportsmen Alliance
countries:
- usa
statesprovinces:
- california
- idaho
- tennessee
- oregon
- arizona
categories:
- hunting
- fishing
tags:
- deer
- hog
- tuna
- salmon
- bass
orgURL: https://www.veteranssportsmanalliance.org
---
The primary mission of VSA is to get injured and disabled Veterans out of the house and hospital, and into the therapeutic surroundings of nature and outdoor adventure.

VSA has enabled our heroes that have suffered double and triple amputations to experience outdoor adventures they most likely would not be capable of doing on their own.

VSA has sponsored deer and hog hunts, as well as tuna, bass, and salmon fishing trips.

VSA is currently organizing kayaking, hiking, and golf trips as well. Even more exciting, the VSA & Hot Rod Service Company have been working on a Mustang Project and will soon be scheduling "Track Day Events," and putting our Veterans behind the wheel at race tracks across America.
