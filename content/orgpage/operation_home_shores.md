---
title: Operation Home Shores
countries:
- usa
statesprovinces:
- florida
categories:
- hunting
- fishing
tags:
- fishing
- hunting
orgURL: http://homeshores.org
---
Operation Home Shores, Inc. is a 100% volunteer organization. All proceeds from any fundraising efforts go 100% to helping veterans find healing.We specialize in kayak fishing adventures, hunting, and camping retreats for Military Veterans.Fishing is a release from the stress. We provide a service FOR VETERANS, BY VETERANS. It is important to raise awareness within our community of the benefits that recreational therapy can have on our Heroes. Helping those who have fought for our freedom by introducing a healthy lifestyle, an outlet for frustration, assistance with re-integration, and personally guided trips with fellow compassionate Veterans.
