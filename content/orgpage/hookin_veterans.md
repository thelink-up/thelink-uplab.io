---
title: Hookin Veterans
countries:
- usa
statesprovinces:
- north carolina
- florida
categories:
- fishing
orgURL: https://hookinveterans.com/
---
The mission at Hookin’ Veterans is simple: to connect an ever-growing community of veterans and their families through a weekend of camaraderie and deep-sea fishing. Since most veterans claim to miss the bond of brotherhood that serving in the armed forces forges, Hookin’ Veterans goal is to help them reconnect with other service members and re-establish this bond in an effort to combat the increase of veteran suicide.

At Hookin’ Veterans we provide, through our generous donors, an all-expense paid weekend fishing trip that is designed to be fun, relaxing and in some ways soothing to the soul of veterans and by extension their families. But we cannot do it alone.
