---
title: Alpha Elite Performance Outdoors
countries:
- mexico
- usa
statesprovinces:
- washington
- texas
categories:
- hunting
- fishing
tags:
- salmon
- hogs
- sailfish
orgURL: https://www.aepoutdoors.com
---
At AEP outdoors, we honor the service of our special operations veterans by providing them with adrenaline outdoor adventure and fellowship. It was that brotherhood while they served that kept them alive and it’s the brotherhood that will continue to do so.  We’re not here to raise awareness, we already know what the problems are, we’re just here providing a solution.

When Alpha Elite Performance was founded in 2015, our team knew we wanted it to be more than just a veteran owned supplement company. Why? Because we know from experience that our war fighters need more than the support from another "veteran owned company" and a slap on the back after service. They need to continue to interact with the brotherhood. We are doing that for our military and veteran community by providing healing outdoor adrenaline adventures and fellowship

We are a nonprofit which is all in to support our SOF veterans and those who support them. We aren't trying to raise awareness. The statistics, studies and research are clear — we all have struggles. But it’s our hope to help you struggle well and not alone.

Our mission isn’t to raise awareness — we want to stand in the gap and be a part of the solution. As an organization, we’ve witnessed the power fellowship and the brotherhood of bonding can do for one's warrior spirit and mental health. It’s our aim to provide the excitement and rush of adrenaline we craved as a team on every mission throughout our careers. That fight or flight response? We want to help you own it again, outside, living life NOT alone.
