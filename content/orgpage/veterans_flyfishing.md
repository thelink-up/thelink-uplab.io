---
title: Veterans Flyfishing
countries:
- usa
statesprovinces:
- georgia
categories:
- fishing
tags:
- trout
- bass
- panfish
- bream
orgURL: https://veteransflyfishing.org
---
Veterans Flyfishing was founded by veterans for veterans. We serve as teachers and guides to any veteran or active duty military who wants to learn the art of fly fishing. Maybe you served 50 years ago, or maybe you’re serving right now, whoever you are and wherever you served – we thank you for your service. And, we want to take you fly fishing.

Veterans Flyfishing is a non-profit chartered under Fly Fishers International.
We:
Serve vets who want to fly fish;
Teach vets who want to advance in fly fishing;
Help vets achieve whatever skill level they choose in fly fishing;
Offer an emotional outlet for vets who need it;
Reach out to other veteran and fly fishing organizations;
Cut the drama and go fishing.
