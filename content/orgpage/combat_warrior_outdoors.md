---
title: Combat Warrior Outdoors
countries:
- usa
statesprovinces:
- ohio
categories:
- hunting
- fishing
- golf
tags:
- pheasant
- duck
- hog
orgURL: https://www.combatwarrioroutdoors.org
---
Combat Warrior Outdoors' mission is to provide outdoor adventures for disabled combat veterans to help them break away from the stresses and anxiety caused by Post Traumatic Stress (PTS), Traumatic Brain Injury (TBI), and also helps them reintegrate back into civilian life.  Our outdoor adventures will provide the opportunity to meet, interact, and connect with like-minded veterans.  It will also allow veterans a chance to enjoy the camaraderie that they may have missed since serving, all while reconnecting with the great outdoors.
