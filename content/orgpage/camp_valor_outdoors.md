---
title: Camp Valor Outdoors
countries:
- usa
statesprovinces:
- missouri
categories:
- fishing
- hunting
- shooting
- archery
- 4-wheeling
orgURL: https://www.campvaloroutdoors.org/
---
Camp Valor Outdoors is a 501(c)(3) non-profit organization dedicated to our military veterans
We recognize and honor ill, injured, and wounded disabled veterans and their families through adaptive and competitive activities such as guided fishing, hunting, shooting, archery, 4-wheeling, or just simply relaxing around the campfire
All branches of the military services, from any generation, are welcome (to include active duty case by case)
We understand the hurdles that veterans and their families face after military service, particularly with injuries and illnesses
Warriors are never alone on the battlefield and shouldn't be alone when they come back home
Healing in the great outdoors with Camp Valor Outdoors and reconnecting with fellow warriors is therapeutic and essential to healing.
