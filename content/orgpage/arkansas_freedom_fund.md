---
title: Arkansas Freedom Fund
countries:
- USA
statesprovinces:
- arkansas
categories:
- hunting
- fishing
tags:
- fishing
- hunting
- golf
- cycling
- hiking
- kayaking
- camping
orgURL: http://www.arkansasfreedomfund.org
---
What are we are doing for the Veterans of Arkansas ?
You know we do outdoor events for Arkansas Veterans.  There is a need to keep the life of a veteran active. We do that through recreational / rehabilitative / social outdoor activity.  Like fishing, golf and cycling. Now we added Hunting to our program!!

We want to help in every aspect of achieving a serviceman's physical goals after the military. We are assisting three of our warriors now in their training in triathlons, endurance riding and martial arts. 
Check our events calender for all current happenings.
Our current projects and goals 
These are continuously in planning stages. If you want to volunteer on any of the below events please contact us.  If you would like to take on a challenging and rewarding program please let us know.  Our board is small but all with big hearts.

Arkansas Freedom Adventure Series - Hiking the Trails of Arkansas
This Event is open to ALL AFF Members, Veterans and family members wanting a team event to explore Arkansas. Guided by own Mickey Spillane, a great way to see Arkansas. Trails are accessible and open for every level of hiker.
