---
title: Hero Expeditions
countries:
- usa
statesprovinces:
- colorado
- wyoming
- nebraska
categories:
- hunting
- fishing
tags:
orgURL: https://heroexpeditions.org
---
At Hero Expeditions we organize outdoor adventures for military service members, veterans and first responders. HEX was founded on the belief that giving back to those that have selflessly sacrificed for all of us is simply the right thing to do.

Our Goal

Organize events that provide an atmosphere of camaraderie and fellowship among our participants.

Our Mission

Help create meaningful relationships that improve morale and promote healing.
