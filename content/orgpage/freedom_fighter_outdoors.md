---
title: Freedom Fighter Outdoors
countries:
- usa
statesprovinces:
- florida
- pennsylvania
categories:
- fishing
- hunting
tags:
- fishing
- hunting
orgURL: https://www.freedomfighteroutdoors.org/about
---
Our goal here at FFO is to raise awareness and seek public aid for needs, services and activities for injured veterans. Our events facilitate injured veterans in assisting each other through teamwork based outdoor recreational activities by providing unique, once in a lifetime outdoor activities. FFO honors and empowers our nations injured service veterans.
