---
title: Fishing for Freedom (Quincy)
countries:
- usa
statesprovinces:
- illinois
categories:
- fishing
tags:
- bass
- crappie
- catfish
orgURL: https://fishingforfreedomquincy.org
---
Fishing For Freedom - Quincy is a no entry fee bass, catfish, and crappie tournament that pairs our Warriors (active Military and Veterans) with our Boaters (professional anglers, avid tournament fishermen, fishing guides, and local outdoorsmen) for a fun day of fishing on our local fisheries. Fishing For Freedom - Quincy cannot give back all the lost time away from the great outdoors during deployment, but we can give back a weekend. We do that by providing our Warriors with a weekend full of entertainment and enjoyment of the great outdoors with their fellow Veterans and active duty personnel, as well as the general public. This event is designed to provide our Warriors with events that range from a fish fry, trapshooting, archery, live music, a Heroes Banquet, and a day of fun tournament fishing on the Mighty Mississippi River or Mark Twain Lake. We also have private lake(s) where we take our Warriors who cannot easily fish from a boat to ensure their safety and well-being.
