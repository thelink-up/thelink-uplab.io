---
title: Chairbound Sportsman
countries:
- usa
statesprovinces:
- utah
categories:
- hunting
- fishing
tags:
orgURL: http://www.chairboundsportsman.org
---
 Chairbound Sportsman is a 501(c)(3) Nonprofit Organization of volunteers who help wheelchair bound and disabled people to have hunting, fishing and outdoor opportunities that they may only dream about. 
