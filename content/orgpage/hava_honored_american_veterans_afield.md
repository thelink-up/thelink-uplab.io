---
title: HAVAHonored American Veterans Afield
countries:
- usa
statesprovinces:
- Massachusetts
categories:
- hunting
- fishing
tags:

orgURL: https://www.honoredveterans.org
---
*Mission*:
The HAVA vision is the creation of a small organization of volunteers from the shooting sports industry to facilitate a series of hunting and shooting activities for groups of disabled veterans wherein personal attention of the sponsors and facility operators contributes to the veteran’s sense of joy and accomplishment, and a permanent awareness that marvelous things are possible despite disabling injuries. These veterans have given their full measure of commitment to the preservation of their country’s values, and deserve America’s contribution to their healing process to whatever degree necessary to accomplish physical, mental and cultural rehabilitation. HAVA, through the efforts of Sustaining Sponsors and other contributors, can become an inspiration to both the veteran and to a grateful nation whose best instincts are to support the veteran who has served its cause so well.
