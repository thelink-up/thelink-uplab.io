---
title: The Outdoor Adventure Foundation
countries:
- usa
statesprovinces:
- North Dakota
- South Dakota
- Florida
- Minnesota
- Washington
- Nevada
categories:
- hunting
- fishing
tags:
- deer
- elk
- moose
- bear
- duck
- geese
- pheasant
- quail
orgURL: https://outdooradventurefoundation.org
---
The Outdoor Adventure Foundation is a 501(c)(3) non profit foundation providing hunting and fishing adventures for children and young adults under the age of 25 with cancer or other life threatening illness, also providing adventures for combat disabled veterans under the age of 40 that are wheelchair bound or missing a limb due to active duty service.
