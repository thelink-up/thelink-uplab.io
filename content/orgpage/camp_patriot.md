---
title: Camp Patriot
countries:
- canada
- usa
statesprovinces:
- montana
- texas
- utah
- alaska
- idaho
categories:
- hunting
- fishing
tags:
- elk
- moose
- deer
- bear
- fishing
orgURL: https://camppatriot.org
---
Camp Patriot's mission:
CHALLENGE - CHANGE & LEAD
Life Changing Adventures, Mentoring, Veteran Leadership Program 

Camp Patriot exists to take combat wounded & disabled U.S. Veterans on outdoor adventures.

The task is monumental. Today, there are over 2.3 million wounded & disabled veterans in the U.S. The number of disabled men and women veterans is growing with each day, as the "war on terror" continues. The suicide rate for veterans is 22+ per day. We need to reach out to the veterans now & with a strong arm!

These brave veterans sacrificed so much in order to ensure our safety and freedom. All of these veterans had dreams about the future, but many of those dreams were lost due to injuries suffered in the line of duty. Outdoor activities that they hoped to do in the future have vanished due to their disability. We want to thank these veterans by showing them that with the right help, they can again enjoy the great outdoors.
