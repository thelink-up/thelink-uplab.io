---
title: "Operation: Fishing Freedom - The Voice of of a Hero"
countries:
- usa
statesprovinces:
- illinois
categories:
- fishing
tags:
orgURL: https://www.operationoutdoorfreedomfoundation.org
---
Ben Olsen and I founded this organization - because we realized just how important our veterans are to the freedom of this country. We feel EVERYDAY is Veterans Day - and we need to recognize veterans - every chance we get!

﻿My Grandfather, Father & Brother all served our country in the US Military. My “life path” didn’t allow for it, so I decided to “Give Back” to those individuals who provide our FREEDOM.

After the tragic events of September 11th, 2001, our country became more “unified” – than I had ever experienced. I’m sorry to say that in just 15-20 “short years later” – again, we as Americans - are divided.

Rather than to continue that divide, I’m making it my life mission to “give back” to the individuals who provided the “right” for us - to live in such a FREE country – that we could express our own opinions/beliefs.
I found it to be my mission – “to make tomorrow better than today” for the TRUE HEROES of our country – our Veterans. I helped co-found the nonprofit – Take a Vet Fishing, NFP in 2011. I wanted to serve veterans whenever I could and introduce them to the great outdoors. There was a study done back in 2003, that highlighted the PROFOUND EFFECTS that fishing specifically (and the outdoors in general) has on Post Traumatic Stress (PTS). So we started a movement to get Veterans on the water and into the outdoors.

Over the years, I have met so many of this country’s greatest war heroes, that I couldn’t let these stories go untold. In 2015, I started “Operation Fishing Freedom Foundation” (NOW – Operation Outdoor Freedom Foundation - Nonprofits) to preserve the stories of our US Military Veterans. In just 5 years, We’ve filmed and documented over 50+ Veterans stories. We are not only “preserving” a small piece of history, we are providing families with a “memory book” that can be handed down to future generations. Through 5 seasons on Discovery Channel, we’ve touched MILLIONS of viewers lives with these HEROIC stories. In addition, the show has become a RESOURCE for veterans to learn about alternative PTS treatments that the VA does not offer. To date, we’ve treated (and paid for) over 100+ Veterans to get relief from their PTS symptoms.

I believe we can never do enough to express our gratitude to our veterans, first responders and front-line workers. While our programs have certainly made a difference in many lives, there is MUCH MORE work to be done. I invite you to join me in the “Mission to Give Back” to our nations greatest Heroes. I encourage you to watch the amazing stories from Seasons 1-6 on our YouTube channel, and hear these stories – first-hand. 
