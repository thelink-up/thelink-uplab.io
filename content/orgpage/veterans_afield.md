---
title: Veterans Afield
countries:
- usa
statesprovinces:
- wisconsin
categories:
- hunting
- fishing
tags:
orgURL: https://www.facebook.com/The-Veterans-Afield-Foundation-1032440060296982/?ref=page_internal
---
These Veterans gave of themselves so we could enjoy the outdoors.  Their disabilities should not restrict them from sharing in the same outdoor enjoyment.  It is the mission of the Veterans Afield U.A. to ensure that they are given the same opportunity.
