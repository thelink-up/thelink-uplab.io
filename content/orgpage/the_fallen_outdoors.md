---
title: The Fallen Outdoors
countries:
- usa
states:
- nationally
categories:
- hunting
- fishing
orgURL: https://thefallenoutdoors.org
---
We are a Veteran 501(c)(3) all volunteer organization established to facilitate hunting and fishing trips for veterans. We aim to connect Soldiers, Airmen, Sailors and Marines, with a network that will serve them locally and nationally.

JOUR OUR ONLINE VETERAN COMMUNITIES.
Join our [Main Community Facebook Group](https://www.facebook.com/groups/1442558026045786/) to keep up with nationwide giveaways and information.
Join our district community groups to get in touch with other local veterans and active service members near you.
[West Coast Community Group](https://www.facebook.com/groups/1089041847842037/)
[East Coast Community Group](https://www.facebook.com/groups/635736996601517/)
[Southern Community Group](https://www.facebook.com/groups/167972426943652/)
[Midwest Community Group](https://www.facebook.com/groups/1829232140644438/)
