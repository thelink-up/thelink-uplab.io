---
title: The Foundation for Exceptional Warriors
countries:
- usa
- New Zealand
statesprovinces:
- Oklahoma
categories:
- hunting
- fishing
tags:
orgURL: https://www.exceptionalwarriors.org
---
Serving those Warriors that epitomize Honor, Valor, and Sacrifice. The FEW defines Exceptional Warriors as Special Operations Forces; those that have received medals for Valor or Heroism; former Prisoners of War and lastly the Combat Wounded of any era.
