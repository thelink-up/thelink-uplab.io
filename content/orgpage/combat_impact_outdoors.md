---
title: Combat Impact Outdoors
countries:
- usa
statesprovinces:
- new mexico
- texas
categories:
- hunting
- fishing
tags:
- guide
- deer
- redfish
- speckled trout
orgURL: https://combatimpact.org
---
Welcome to Combat Impact Outdoors! We are a non-profit organization established by a father and son , who are both military veterans and avid outdoorsmen. Combat Impact Outdoors is dedicated to helping our combat verterans enjoy the great outdoors and improve their mental health and well-being. We offer a comprehensive suite of services for hunting and fishing trips for veterans, so they can enjoy and appreciate life. So if you’re a veteran looking for some cost-free outdoor fun, or need guidance and resources with filing Veteran benefits claims, be sure to contact us through our Application form.
