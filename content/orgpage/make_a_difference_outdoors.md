---
title: Make A Difference Outdoors
countries:
- usa
statesprovinces:
- california
- Montana
- idaho
categories:
- hunting
- fishing
tags:
- ducks
- dove
- deep sea fishing
- deer
orgURL: https://madoutdoors.org
---
M.A.D. Outdoors was created with the purpose of honoring combat-injured veterans with more than a "thank you" by offering these brave individuals a variety of activities in the great outdoors. Knowing that many injuries are not seen, efforts quickly expanded to provide shooting, hunting and fishing activities to all veterans, as well as individuals of all ages with physical challenges and special needs.
