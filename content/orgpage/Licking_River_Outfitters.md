---
title: Licking River Outfitters
countries:
- usa
statesprovinces:
- kentucky
categories:
- hunting
- fishing
tags:

orgURL: https://lickingriveroutfitters.org
---
Licking River Outfitters, Inc. (LRO) is a 501(c)(3) non-profit that specializes in sending wounded soldiers, disabled veterans, critically ill and disabled youth on the hunting adventure of their dreams! LRO outings give our youth and veterans something to look forward to and help sustain them in their time of need. They can forget about the world of hospitals, and high medical bills by participating in a real fair chase trophy hunting adventure. LRO is composed of a 100% volunteer staff comprised of veterans and civilians from all walks of life. By working cooperatively with hunt donors as well as generous individual and corporate sponsors, LRO makes a significant impact on the lives of thousands of children, disabled veterans, and their families. LRO's primary purpose is to conserve and promote the use of our natural resources by providing opportunities for hunting, fishing, trapping, education, and habitat enhancement.
