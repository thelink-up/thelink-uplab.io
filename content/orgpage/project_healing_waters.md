---
title: Project Healing Waters
countries:
- usa
statesprovinces:
- alabama
- alaska
- arizona
- arkansas
- california
- colorado
- connecticut
- delaware
- florida
- georgia
- hawaii
- idaho
- illinois
- indiana
- iowa
- kansas
- kentucky
- louisiana
- maine
- maryland
- massachusetts
- michigan
- minnesota
- mississippi
- missouri
- montana
- nebraska
- nevada
- new hampshire
- new jersey
- new mexico
- new york
- north carolina
- north dakota
- ohio
- oklahoma
- oregon
- pennsylvania
- rhode island
- south carolina
- south dakota
- tennessee
- texas
- utah
- vermont
- virginia
- washington
- west virginia
- wisconsin
- wyoming
categories:
- fishing
orgURL: http://www.projecthealingwaters.org/
---
## Mission Statement
Project Healing Waters Fly Fishing, Inc. is dedicated to the physical and emotional rehabilitation of disabled active military service personnel and disabled veterans through fly fishing and associated activities including education and outings.

Project Healing Waters Fly Fishing (PHWFF) is a 501(c)(3) non-profit organization incorporated in the State of Maryland. PHWFF receives no government funding and is dependent on tax-deductible, charitable donations and the help of numerous volunteers to meet the educational, equipment, transportation, and related needs of its participants.

Project Healing Waters Fly Fishing, Inc. strives to effectively serve the deserving past and present members of our armed forces who have made great sacrifices in the service of our nation.


