---
title: Cedar Creek Veterans Foundation
countries:
- usa
statesprovinces:
- texas
categories:
- fundraising
- fishing
tags: 
- white bass
orgURL: https://www.ccveteransfoundation.org
---
CCVF has been established to raise monies to assist with the physical and emotional recovery and rehabilitation of wounded, injured, and disabled military personnel and veterans with a focus on those individuals residing in and organizations serving the Northeast Texas region
The three charities supported by CCVF also assist with the needs of spouses and other family members.
The principal fundraising activities for CCVF are the Thunder Over Cedar Creek Lake airshow and the and the annual golf classic
Currently CCVF supports Fisher House, Hope For The Warriors and Navy-Marine Corps Relief Society.
