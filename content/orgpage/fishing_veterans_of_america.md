---
title: Fishing Veterans of America
countries:
- usa
statesprovinces:
- oregon
categories:
- fishing
orgURL: https://www.facebook.com/FishingVeteransofAmerica/
---
Providing veterans and people suffering with disablement pathways to alternate healing through adventure sports.
