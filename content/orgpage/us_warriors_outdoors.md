---
title: US Warriors Outdoors
countries:
- usa
statesprovinces:
- colorado
categories:
- hunting
- fishing
tags:
- elk
- deer
- turkey
- upland bird
- waterfowl
- ducks
orgURL: http://uswarriorsoutdoors.org/us/0
---
U.S. Warriors Outdoors, a tax-exempt 501(C)3 nonprofit organization, was created to enable Wounded Warrior Veterans, opportunities to get back to the outdoors and provide these Men and Women reconnections with their hunting dreams and passions.
We provide full financial support, logistics, accommodations, guiding and support resources to bring our wounded heroes on hunts for elk, whitetail deer, turkeys, coyote’s, upland game birds and waterfowl.
