---
title: B4 Outdoors
countries:
- usa
statesprovinces:
- North Carolina
- missouri
- Colorado
- minnesota
- michegan
- south carolina
categories:
- hunting
- fishing
tags:
- guide
- outfitter
orgURL: http://b4outdoors.com
---
B4 Outdoors is a 501c3 non-profit organization that provides veterans, wounded war heroes, at-risk youth, and the disadvantaged and disabled with life-changing outdoor experiences through hunting and fishing.  B4 is run by Kevin Rhodes, a 2017 graduate of the Colorado Outdoor Adventure Guide School, who has almost 30 years of hunting and fishing experience.  He is assisted with the mission of B4 by a group of like-minded hunters, anglers, landowners, guides and outfitters.

The Mission of B4 is to help rehabilitate veterans, first responders, law enforcement officers and youth through hunting and fishing adventures. Below are some pictures from our “mission” hunts which includes our annual Veteran-1st Responder Antlerless Deer Hunt at our lodge in Missouri, individual deer and turkey hunts also at our lodge in Missouri, and countless other hunts and fishing trips all across the country. Some of which include bear hunts in eastern North Carolina, elk hunts in Colorado, ice fishing in Minnesota and Michigan, spoonbill fishing at Lake of the Ozarks in central Missouri, and deer, turkey and upland bird hunts in South Carolina just to name a few.

If you are a veteran, 1st responder, law enforcement officer or youth hunter and would like to go on one of our “mission” hunts or fishing trips, please fill out the hunt application on our website and we will enter you into any available annual hunt giveaways that we award to many deserving individuals every year.

If you are a landowner, hunting or fishing guide, outfitter, or hunt club president and would like to host one of our hunts or fishing adventures, please reach out to us via our CONTACT page. You don’t have to have a fancy lodge or fancy boat to host one of our adventures. All you need is the desire to help us fulfill our mission of helping veterans, first responders, law enforcement officers and kids. All donated hunting and fishing adventures are 100% tax deductible.
B4 will also offer paid hunts, fishing trips, and other outdoor adventures to anyone who is interested.  The profits raised from the paid hunts will be donated back to help fulfill the main mission of B4 Outdoors.
