---
title: Louisiana Hunters for Heroes
countries:
- usa
statesprovinces:
- louisiana
categories:
- hunting
- fishing
tags:
orgURL: http://www.louisianahuntersforheroes.org
---
We’re Louisiana Hunters for Heroes. 
We’re a nonprofit organization that assists our veterans by taking them on guided hunting & fishing trips and offering fellowship & assistance to those in need.
