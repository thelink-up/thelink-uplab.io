---
title: Tenacity Outdoors
countries:
- usa
statesprovinces:
- oregon
- washington
categories:
- fishing
tags:
- guide
- outfitter
orgURL: https://www.facebook.com/profile.php?id=100083533304747
---
Veteran owned and operated. Let’s go fishing!  Tenacity Outdoors operated by U.S. Army veterans and VFW life member John Kaiser Jr
