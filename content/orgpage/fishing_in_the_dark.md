---
title: Fishing in the Dark
countries:
- usa
statesprovinces:
- texas
categories:
- fishing
tags:
- bass
orgURL: https://fishinginthedark.us
---
Fishing in the Dark was created to help military veterans who suffer from PTSD. Our mission is to provide disabled veterans with an opportunity to have an expense free day of bass fishing. We want to spread the message of how having a hobby can positively impact the lives of those with PTSD. We understand that Fishing in the Dark will not be the solution for every veteran, but if we can help prevent one veteran from committing suicide, then all of our work has been worth it. At least 22 veterans a day are giving up on their battle with PTSD. Fishing allows the mind to have something positive to focus on. It can help veterans heal psychologically, emotionally, and even physically.
Having an outlet to focus your time and energy on can keep suicidal thoughts and actions at bay. This is why every veteran that goes out with Fishing in the Dark will be provided with fishing equipment to continue fishing on their own time. This kit will include a rod, reel, tackle and a fishing box to store it all in! If a veteran does not know how to fish, they will be provided with simple easy instructions to get them started.
