---
title: Operation Never Forgotten
countries:
- usa
statesprovinces:
- montana
categories:
- hunting
- fishing
tags:
orgURL: https://operationneverforgotten.org
---
Operation Never Forgotten (ONF) is a national nonpartisan, 100% volunteer non-profit organization. Its mission is to remember, thank and help rebuild the lives of those Post-9/11 veterans and military families who have sacrificed so greatly to defend our freedoms. This is done through awareness campaigns, workshops for veterans with invisible wounds, outdoor retreats for veterans with physical and/or invisible wounds, and training for veteran mentors. All workshops and retreats are also for caregivers. 
“Sports, Afield & Stream (S.A.S.)” outdoor retreats offer veterans once-in-a-lifetime recreational opportunities to include back country wilderness horseback adventures, big game hunts, fly fishing trips, cattle ranch vacations, ice climbing, llama treks, dog mushing, whitewater expeditions, rock climbing, ziplining and more.
