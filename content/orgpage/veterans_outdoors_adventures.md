---
title: Veterans Outdoors Adventures
countries:
- usa
statesprovinces:
- north dakota
categories:
- hunting
- fishing
tags:
orgURL: https://www.veteransoutdoors.org
---
About:

We are a Non-Profit 501c3 Organization started up by 2 Former Active Duty Members of The United States Armed Forces, a Former Peace Officer and a EMT. We wanted to create an opportunity to Veterans in North Dakota, and being majority of our Sponsors are with in the State of North Dakota, it awards us an opportunity to give back to them with Sponsors notification at our events and on our hunts with their logos on our T-Shirts, and Banners.
We want to support these Veterans with other Avenues to deal with "After Active Service" issues, give them an outlet to get in the outdoors and heal, find another Veteran that can relate and bond with. Offer help to other Veteran Spouses who deal with the very same issue's as we do. Help to decrease the Suicide rate with in the Veterans after being released from active service.
We also want to give back to the community that gives to us, by inviting a North Dakota Peace Officer, Firefighter, and First Responder on our Hunts. Volunteer at a Homeless Shelter, and even make a meal to say thanks to these dedicated people mentioned above... because while we were gone defending our country they stayed back and protected our Family's and Communities!

