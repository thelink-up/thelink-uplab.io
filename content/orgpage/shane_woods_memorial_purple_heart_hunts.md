---
title: Shane Woods Memorial Purple Heart Hunts
countries:
- usa
statesprovinces:
- alaska
categories:
- hunting
tags:
- big game
- wounded veterans
orgURL: http://purplehearthunts.com/index.html
---
Spc. Shane W. Woods gave his life while serving his country in the cause of freedom during combat operations near Ar Ramadi, Iraq on August 9, 2006. Shane was a man of outstanding character, with a strong commitment to Duty and Honor. He grew up hunting, fishing, and hiking in the wilds of Alaska, and was proud to defend the freedoms that we enjoy here at home.

Shane Woods Memorial Purple Heart Hunts , Inc. was formed as a way to honor the sacrifices of our nation's heroes. We believe in the healing and restorative properties of spending time in the outdor activities that are a large part of our national heritage. It is our wish to help share some of the passion and enthusiasm with which Shane lived by providing guided Alaskan big-game hunting opportunities to Global War on Terrorism veterans that have been wounded in the call of duty.

