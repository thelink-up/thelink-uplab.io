---
title: Wings of Valor Lodge
countries:
- usa
statesprovinces:
- south dakota
categories:
- hunting
tags:
- pheasant
- outfitter
orgURL: https://www.wingsofvalorlodge.org
---
Wings of Valor is a lodge unlike any other.  Located on some of the best habitat in eastern South Dakota, we are just 40 minutes from Sioux Falls and we offer world-class pheasant hunting.    Wings of Valor operates on the mission of supporting those who have served by providing a gathering place to motivate and empower veterans in the outdoors.  This 501(c)3 non-profit is a fully-accessible commercial hunting lodge available to all to book a great South Dakota pheasant hunt.  Your corporate or family hunt will support the mission of bringing our veterans home.  We host Disabled Veterans hunts twice a month and hunt commercially the remainder. Join us by calling 605-297-4868 and making your reservation for this fall!  It's time to enjoy freedom and support our veterans by visiting Wings of Valor Lodge!
