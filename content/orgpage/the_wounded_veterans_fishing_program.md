---
title: The Wounded Veterans Fishing Program
countries:
- usa
statesprovinces:
- washington
categories:
- hunting
- fishing
tags:
orgURL: https://missionoutdoors.org
---
We believe hope grows outdoors. On the water. On a hunt. On the slopes. On a trail. But we’re about more than just fishing and hunting — those are on the gateway. We’re about connecting and building relationships and trust with our veterans, helping them to get engaged in their community, to discover the immense value of their lives, and showing them there is hope.

Mission Outdoors  is committed to serve the ones who served for us. Our focus is on the camaraderie and community with like minded individuals. We find it just as important for our veterans and active duty to not financially worry for these events and opportunities.

It’s about giving vets a purpose and something to look forward to. It’s about helping them see that it’s not all about what they’re dealing with today, but about looking forward to tomorrow.
