---
title: Outdoors for our Heroes
countries:
- usa
statesprovinces:
- washington
categories:
- hunting
tags:
- turkey
- pigs
- pheasant
- coyote
orgURL: https://outdoorsforourheroes.org
---
Outdoors For Our Heroes is a 501(c)(3) nonprofit organization focused on providing service-connected disabled veterans the opportunity to hunt for a wide variety of animals on quality properties for lifetime experiences.

Our team consists of volunteers, donors, and corporate sponsors that share a passion for hunting, the outdoors, and the desire to enable our service-connected disabled veterans the best opportunities to get out and hunt on quality properties with people who ultimately respect our heroes. It is our obligation to take care of those service men and women that make it home and provide them with hope, optimism and motivation through hunting experiences.

We are an all-volunteer organization. Our staff does not collect any personal monetary compensation for their time. All donations received go to support the outdoor activities.
