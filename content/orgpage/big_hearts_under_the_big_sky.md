---
title: Big Hearts Under the Big Sky
countries:
- usa
statesprovinces:
- montana
categories:
- hunting
- fishing
- recreational
orgURL: https://www.bigheartsmt.org
---
Big Hearts under the Big Sky is a program started by the Montana Outfitters and Guides Association (MOGA)
MOGA places children diagnosed with life threatening illness, women suffering from breast cancer and military veterans who have provided extraordinary service to our country, on fully guided and outfitted trips at no charge.
To meet the mission of the Big Hearts program we partner with several well-known national charities
These MOGA partners preform a vital function of identifying qualified persons who desire and will benefit from an outfitted adventure trip of a lifetime in Montana
MOGA works to match their needs with the right outfitters and guides to fulfill their dreams
In some cases our partners may also play an equally important role in helping execute the trip.
