---
title: Camp Hope
countries:
- usa
statesprovinces:
- missouri
categories:
- skeet shoot
- hunting
- fishing
- hiking
tags:
- turkey
- deer
orgURL: http://www.camphopeusa.org
---
Allow wounded warriors the opportunity to participate in outdoor activities with dignity - to not think about what they "can't" do, only to think what they want to do.  Soldiers can shoot skeet, hunt turkey and deer, fish, hike, explore the country, or relax around the ever-burning firepit. More than 42,000 Soldiers have been wounded in the War on Terror. Camp Hope is committed to letting them heal through nature. Since 2007, dozens of Soldiers - male and female, representing all branches of the military, from across the country, have experienced, enjoyed, and sought solace at Camp Hope.
