---
title: Wounded Veterans Waterfowl Club
countries:
- usa
statesprovinces:
- wisconsin
- colorado
- washington
categories:
- hunting
tags:
- ducks
- geese
- waterfowl
orgURL: https://woundedveteranswaterfowlclub.com
---
The Wounded Veteran’s Waterfowl Club (WVWC) is a Not-for-profit organization providing FREE waterfowl hunts to all Active Duty, Retired, Medically Retired, Medically Separated, US Veterans and Purple Heart Recipients. Wounded Veteran’s Waterfowl Club hunts are open to all branches of the military.
