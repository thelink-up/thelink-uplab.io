---
title: Patriots Cove
countries:
- usa
statesprovinces:
- pennsylvania
categories:
- hunting
- fishing
tags:
orgURL: https://www.patriotscove.org
---
Patriots Cove is a non-profit 501(c)(3) organization in northeastern Pennsylvania and founded by a military veteran and his spouse to empower other veterans, first responders, and their caregivers to heal and adapt to life after service. Through events on our eighteen acre refuge, we provide restorative outdoor activities, environmental service projects, and educational events and retreats for caregivers.
 
