---
title: Wounded Heroes Hunting Camp
countries:
- usa
statesprovinces:
- alabama
- alaska
- arizona
- arkansas
- california
- colorado
- connecticut
- delaware
- florida
- georgia
- hawaii
- idaho
- illinois
- indiana
- iowa
- kansas
- kentucky
- louisiana
- maine
- maryland
- massachusetts
- michigan
- minnesota
- mississippi
- missouri
- montana
- nebraska
- nevada
- new hampshire
- new jersey
- new mexico
- new york
- north carolina
- north dakota
- ohio
- oklahoma
- oregon
- pennsylvania
- rhode island
- south carolina
- south dakota
- tennessee
- texas
- utah
- vermont
- virginia
- washington
- west virginia
- wisconsin
- wyoming
categories:
- hunting
- fishing
tags:
- deer
- elk
- mountain lion
- rockfish
- tuna
- turkey
orgURL: https://www.whhc.org
---
A Service for Our Military Service Members
Offering our wounded veterans the opportunity to get outdoors and enjoy a hunting or fishing trip with all expenses paid, our group focuses on uniting fellow Wounded Heroes on our outdoor adventures. On our trips, our Heroes create memories and friendships that will last a lifetime. Wounded Heroes Hunting Camp is here for our Heroes, and we hope that you will share in our passion and commitment to do what we can for the brave men and women who have sacrificed to protect our freedoms. We believe that healing begins on the hunt! ​​

