---
title: Charlie 22 Outdoors
countries:
- usa
statesprovinces:
- missouri
categories:
- hunting
orgURL: https://charlie22outdoors.com
---
# Mission
Provide outdoor activities, namely hunts, to our nation’s veterans with the goal of showing them there is hope, love and a personal meaning in God’s grace.

Charlie 22 Outdoors was founded, as a result of our passion, to “Serve those who have already Served.” We are a 501(c)3 non-profit organization based out of Webb City, Missouri
We have no paid staff and our Board of Directors are all volunteers
You may or may not be aware of this, but every day in our country 22 veterans commit suicide
That total is more since 9/11 than all together the number killed in action from the Korean War through today’s conflict in the Middle East
This has to stop
The PTSD and the personal demons they face are very real
We provide hunts free of charge to those we serve and their families
We cover all expenses including travel, lodging, food, tags, guides, taxidermy, and meat processing
We believe that healing can take place when a relationship with Jesus Christ is formed.
