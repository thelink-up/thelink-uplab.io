---
title: Base Camp 40, Warrior's in the Wild
countries:
- usa
statesprovinces:
- colorado
- texas
categories:
- hunting
- fishing
tags:
- pheasant
- elk
orgURL: https://bc40hunts.org
---
Since the first hunt in 2011, Base Camp 40, Warriors In The Wild has hosted over 100 Veterans on all expenses paid outdoor adventures across the country and British Columbia. Powered by the cornerstone of gracious landowners, amazing sponsors, and dedicated volunteers who embrace the meaning of giving back, BC40 forges ahead to give back to our nation’s brave Veterans via the "Gift Of The Wild". It is where, what once was thought to be lost, can be rediscovered. We simply open doors that inspire the resurrection of hope and faith, for the beauty in this life never leaves. It is only our perspective of our world that changes.
