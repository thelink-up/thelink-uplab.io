---
title: Kentucky Wounded Heroes Inc
countries:
- usa
statesprovinces:
- kentucky
- tennessee
categories:
- hunting
- fishing
tags:
orgURL: https://kentuckywoundedheroes.net
---
Kentucky Wounded Heroes is a 501(c)(3) non-profit entity that exists to serve Kentucky’s current or former members of the United States Armed Forces, sworn law enforcement, sworn firefighters, and sworn or affirmed EMS, that have been injured during combat operations or line-of-duty service.  Our mission is to honor and empower the emotional wellness of these heroes through outdoor activities, programs, and services that enable them to reconnect to nature’s healing properties.
