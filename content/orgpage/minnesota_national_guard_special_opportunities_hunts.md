---
title: Minnesota National Guard Special Opportunities Hunts
countries:
- usa
statesprovinces:
- minnesota
categories:
- hunting
- fishing
tags:
- turkey
- deer
- archery
orgURL: http://mnngapps.azurewebsites.us/huntingform.php?
---


Available hunts include an Archery Deer Hunt, a Muzzle Loader Deer Hunt and a Turkey Hunt. A separate Archery Deer Hunt and Turkey Hunt will be held at both [Camp Ripley](http://www.minnesotanationalguard.org/camp_ripley/) and the [Arden Hills Army Training Site](http://www.minnesotanationalguard.org/ardenhills/index.php). The Muzzle Loader Deer Hunt is only available to previously deployed Service memberswill be hosted at Camp Ripley Only.


