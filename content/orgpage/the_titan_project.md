---
title: The Titan Project
countries:
- usa
statesprovinces:
- iowa
categories:
- fishing
tags:
orgURL: https://www.thetitanproject.org/reelinginvets
---
Reeling in Vets is "teaching a man or woman to fish" in terms of spending time with other veterans and sharing stories, ideas and creating a support network. All while building a custom fishing pole. The take-away is not only a fishing pole. It is a one of a kind piece made by the veteran. A symbol of accomplishment, bonding and creativity. It will allow our comrades in arms to step into the outdoors or to be given as a gift to a friend or family member. Check our events page to see upcoming dates.
