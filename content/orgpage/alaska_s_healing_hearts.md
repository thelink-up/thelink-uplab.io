---
title: Alaska's Healing Hearts
countries:
- USA
statesprovinces:
- alaska
categories:
- hunting
- fishing
- outdoor
orgURL: http://www.alaskashealinghearts.com/
---
Alaska's Healing Hearts year round nationwide outdoor programs include hunting, fishing, and other outdoor recreational programs for our nations brave wounded warriors, these activities are termed "social reintegration" by therapists. They are not just the clinical rehabilitation programs which injured military personnel usually participate in; they also provide disabled Veterans with hope that they can live active and productive lives. Based on our own experiences these programs help bolster confidence and self esteem, while serving as a venue for them to assimilate with the public. Thanks to donations from people like yourself and corporate sponsors we are able to continue these programs at virtually no cost to our wounded heroes, Thank you.

Alaska's Healing Hearts is a 501(c)(3) tax-exempt #27-4036000, non-profit organization.
