---
title: America's HERO foundation (A HERO)
countries:
- usa
statesprovinces:
- alabama
categories:
- fishing
- golfing
- flyfishing
- bear
- deer
- hunting
orgURL: http://www.aherousa.com
---
AHERO connects Veterans through outdoor activities as a means to recover from their physical wounds and psychological trauma to reintegrate with American life. Tax deductible contributions are dedicated to AHERO programming and we operate on a 100% all-volunteer staff.

AHERO (America's Heroes Enjoying Recreation Outdoors) connects Veterans with patriotic members of local communities by organizing outdoor events and social activities. Our goal is to heal the physical and psychological wounds of war and military service by:

Introducing Veterans to resources and programs available to them to increase their overall quality of life
Developing an informal support network of Veterans across the country
Encouraging constructive communication and engagement
Boosting morale
AHERO was organized and is operated by OIF and OEF Marine Corps and Army Veterans who understand the challenges today's war Veterans face in reengaging the civilian world.
