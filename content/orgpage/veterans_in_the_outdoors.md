---
title: Veterans In the Outdoors
countries:
- usa
statesprovinces:
- minnesota
- south dakota
categories:
- hunting
- fishing
tags:
orgURL: https://www.veteransintheoutdoors.org
---
Veterans In The Outdoors (VITO) is a 501(c)3 non-profit organization that allows
individual veterans, drawn at random, an opportunity to experience hunting and fishing. The lucky individual will participate in hunting or fishing (depending on season) when it works within their schedule.

Veterans In The Outdoors thrives to spread the enjoyment of the outdoors to past and present
service members of the armed forces through hunting/fishing programs and
events.
