---
title: Horicon Marsh Wounded Warriors
countries:
- usa
statesprovinces:
- wisconsin
categories:
- hunting
tags:
orgURL: http://horiconmarshww.com/index.html
---
We are a small group of volunteers that has found a way to give back to our heroes.

Our mission is to give our American hero’s a weekend of guided duck hunting on the storied Horicon Marsh--home cooked meals, brotherhood healing and sincere gratitude for everything they have given us. Thanks to the generosity of donors to date we’ve been able to provide a duck hunting weekend filled with relaxation, laughter and healing to over 80 veterans. And some years, even a duck or two.

