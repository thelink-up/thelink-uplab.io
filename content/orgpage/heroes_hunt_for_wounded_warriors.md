---
title: Heroes Hunt for Veterans
countries:
- usa
statesprovinces:
- wisconsin
categories:
- hunting
- fishing
tags:
orgURL: http://heroeshuntforvets.org
---
Heroes’ Hunt for Veterans would like to thank all of our wonderful Veterans who put their life on the line to protect our freedom!  We love you and can never say THANK YOU enough!

Each Saturday we’ll meet our hunters at the AmericInn Hotel & Suites in Waupun, get settled in, then its off to the shooting range for some target practice and getting to know each other. When the time is right we will head out to one of our many blinds to include three Redneck blinds, two constructed 8×8 blinds, six pop up blinds, or one of 35 ladder stands. Each hunter will be accompanied by a guide whose job it is to make this about the hunter and his/her experience. Guides will be filming the hunt so the memory can be shared with others.
