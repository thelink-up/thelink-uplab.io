---
title: Pennsylvania Outdoor Veterans
countries:
- usa
statesprovinces:
- Pennsylvania
categories:
- hunting
- fishing
tags:
orgURL: https://paoutdoorveterans.org/family-programs
---
In 2015, Pennsylvania Outdoor Veterans, Inc. was founded by Ryan Bowman, a combat veteran of Operation Iraqi Freedom. His creation of the organization was a response to the tragic number of suicides among combat veterans. Every day over 22 veterans take their own lives. The mission of Pennsylvania Outdoor Veterans is to reduce veteran suicide through the healing nature and camaraderie developed during outdoor activities such as camping, hunting and fishing. It has been documented that reconnecting with nature has a strong healing effect on those that have been traumatized by combat and other life shattering events. Nature is not just a backdrop for health and healing activities, but rather a vital part of the healing process.

Bringing combat veterans together, will forge friendships and support systems that will be available when post traumatic stress kicks in: providing a source of hope and light to the Veteran. Just knowing that they are not alone; that we as a community stand with them will provide a glimpse of hope that can prevent a desperate act of a soul in unbearable pain. This is our mission, but we cannot do it alone. We need your support to help stop veteran suicides.

Pennsylvania Outdoor Veterans, Inc. is a volunteer, non-profit organization, which will use your donations for programs, activities, and events to benefit Veterans and their families. Help show our Veterans they are not alone. Help us make a difference today.
