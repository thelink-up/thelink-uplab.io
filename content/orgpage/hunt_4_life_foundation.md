---
title: Hunt 4 Life Foundation
countries:
- usa
statesprovinces:
- wisconsin
categories:
- hunting
tags:
orgURL: https://www.hunt4lifefoundation.org/about-us/

---
At Hunt 4 Life Foundation we know that at some point every person is searching for their true purpose in life. Why does God have us here? We believe the total answer to that question can be simply found in God’s two greatest commandments. First, to love God with all your heart, soul and mind, and the second, to love your neighbor as yourself (Matthew 22:36-39). We believe the more we focus on living out these two commandments, the more fulfilled and purposeful our lives will be. So, simply stated, at Hunt 4 Life Foundation, we believe we are all on a Hunt 4 Life! Come and join us on the hunt!
