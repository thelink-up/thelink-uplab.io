---
title: Purple Heart Outdoors
countries:
- usa
statesprovinces:
- louisiana
categories:
- hunting
- fishing
tags:
orgURL: http://www.purpleheartoutdoors.org
---
Purple Heart Outdoors is a non-profit organization that was built to support and conduct outdoor activities for service men and women, and disabled veterans. Our mission is to reconnect our brave warriors with the outdoors by offering them once in a lifetime opportunities to go hunting and fishing.

​Our plans are to host skeet shoots, fishing tournaments and other fundraisers to help raise money to send these heroes back into the outdoors. Through these outdoor activities, we at Purple Heart Outdoors are going to provide these warriors with hope so they can live more active and productive lives. We want to provide activities that will allow them something to look forward to.
