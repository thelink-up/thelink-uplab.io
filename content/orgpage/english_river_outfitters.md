---
title: English River Outfitters
countries:
- usa
statesprovinces:
- iowa
categories:
- hunting
- fishing
tags:
- hunting
- fishing
orgURL: https://englishriveroutfitters.org
---
We believe there is healing in nature; therefore we emphasize an outdoor experience where veterans can fish, hunt, and hike in the woodlands surrounding our facilities. Veterans with physical disabilities are able to participate fully using our all-terrain wheelchairs. The veterans we serve participate in sporting activities that challenge and empower them, all while immersed in Iowa’s natural, refreshing wilderness.
