---
title: Montana Wounded Warriors
countries:
- usa
statesprovinces:
- montana
categories:
- hunting
- fishing
tags:
orgURL: https://montanawoundedwarriors.org
---
Our mission is to take groups of 2 to 6 wounded Montana Veterans on all-expense-paid hunting and fishing trips where their chances of success are extremely high.

We focus solely on putting these wounded Veterans in a beautiful outdoor setting and giving them the opportunity to bond with others who have walked in their boots.  Other organizations may have more resources and skills at dealing with other aspects of these wounded Veterans’ needs but we feel that the abundant natural resources available in Montana combined with a willing group of outdoor enthusiasts allows us to provide these Veterans with an unforgettable, high-quality experience.

These outdoor excursions serve many purposes, including:

Giving others the chance to thank these men and women for the sacrifice they have made for our country.
Giving severely injured service people the chance to get out and participate in activities that they may have felt that they would never be able to do again due to their disabilities
Giving Veterans another opportunity to heal.
