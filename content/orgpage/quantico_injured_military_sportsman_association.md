---
title: Quantico Injured Military Sportsman Association
countries:
- usa
statesprovinces:
- virginia
categories:
- hunting
- fishing
tags:
orgURL: http://www.qimsa.org
---
QIMSA provides all-encompassing, expense-free outdoor experiences to wounded warriors from all branches of service, regardless of injury or hunting and fishing experience. QIMSA is a federally-approved, non-profit, 501(c)(3), volunteer program operated on Marine Corps Base, Quantico, Virginia. Our program provides combat-injured military members undergoing treatment at Walter Reed, Bethesda, or any other hospital in the nation’s capital region the opportunity to realize their hunting and fishing potential, regardless of injury, in the company of fellow veterans and sportsmen.

