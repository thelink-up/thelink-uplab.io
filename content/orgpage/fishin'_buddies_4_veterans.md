---
title: Fishin' Buddies 4 Veterans
countries:
- usa
statesprovinces:
- california
categories:
- fishing
tags:
orgURL: https://fb4veterans.square.site
---
We are a network of veterans and anglers joining forces to combat a national epidemic, suicide among our Nation's veterans and military personnel. Our goal is to help them heal through fishing and support them through their good and bad times. A few of us here are living proof of the positive effects that fishing has had on our lives. This has provided the inspiration needed to spread the word. Fishing provides peace, serenity and escape from the everyday hustle of life and those hidden wounds many of our Nation's veterans carry around. We are trading our pills for reels and going from battle buddies to fishing buddies. Whether you came here for help or to help, we welcome you with open tackle boxes. We got your six; nobody is left behind. Relax, reflect, rejuvenate, recover, and restore. Ready-Aim-Cast
