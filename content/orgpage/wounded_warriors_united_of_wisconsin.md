---
title: Wounded Warriors United of Wisconsin
countries:
- usa
statesprovinces:
- wisconsin
categories:
- hunting
- fishing
tags:
orgURL: https://www.woundedwarriorsunitedwi.org
---
Wounded Warriors United of Wisconsin is a 501 c (3) nonprofit organization dedicated to working with Wisconsin veterans. Whether deployed or stateside, we work with all veterans. The veterans who participate in our outings have the opportunity to enjoy and engage in activities they have always had a passion for. 

All expenses for the hunting and fishing trips are covered and the participating veterans never pay. Our volunteers and events help uplift their spirits and inspire our veterans
