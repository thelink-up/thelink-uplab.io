---
title: Wounded Warrior Outdoors
countries:
- usa
statesprovinces:
- nationwide
categories:
- hunting
- fishing
orgURL: http://woundedwarrioroutdoors.com/
---
Wounded Warrior Outdoors, Inc. is a non-profit organization exclusively founded to provide wounded servicemen and women with therapeutic outdoor adventures across North America. WWO gives deserving Active Duty Warriors in transition the opportunity of a lifetime in the wilderness location of their choice. We call them “Adventures Enabled.” Their adventure could take them bear hunting in the mountains of British Columbia, Canada, on alligator hunts or fishing excursions in the Gulf Coast region of Florida or deer hunting in Texas. During their experience, they will participate in therapeutic activities such as backpacking, trail expeditions and numerous social interactions.

Wounded Warrior Outdoors provides this all-inclusive adventure at absolutely no cost to the servicemen and women, their families or the government. Transportation, lodging, meals and documentation of the adventure is provided free of charge. It is because of this arrangement that Wounded Warrior Outdoors relies entirely on private donations. Administration, general offices and personnel services are donated.
In order to maintain this highly effective organization, we need the support of private individuals and corporations. At Wounded Warrior Outdoors, over ninety percent of all donations directly benefit these Active Duty Warriors in transition.

Fifty wounded, but active, members of the military are selected to participate each year. Our host facilities are of the highest quality and offer all the comforts of home. WWO also provides complete outfitting including gear, clothing and field transport. Even taxidermy services are provided at no charge.

Most important to the program, Active Duty Warriors in transition are provided with all the accessibility and equipment necessary to make it an adventure that is truly memorable and one in which they can participate fully. No matter the individual’s disability, our focus in on ability and making sure their adventure becomes the memory of a lifetime.
All military branches have participated in our program and we have many relationships with military hospitals that help us facilitate our programs.
