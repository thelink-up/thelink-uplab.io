---
title: Fishing Has No Boundaries
countries:
- usa
statesprovinces:
- iowa
- minnesota
- north dakota
- wisconsin
- illinois
- indiana
- colorado
- kansas
categories:
- fishing
tags:
orgURL: https://fhnbinc.org
---
To provide recreational fishing opportunities for all anglers with disabilities regardless of their age, race, gender, or disability.
To create public awareness of the problems facing persons with disabilities and the shortfall of recreational opportunities.
To be a resource for information on adaptive fishing equipment and accessibility.
To promote research & development of specialized adaptive equipment and gear to enhance fishing experiences for anglers with disabilities.
To help fulfill the needs of participants for independence, positive self image, and a genuine feeling of accomplishment.
To enhance the lives of participants with camaraderie and lasting friendships through FHNB events across the country.
To provide information to any person with interest in setting up a new chapter or fishing event for anglers with disabilities.
To promote the need for recreation for persons with disabilities in a positive way.
To educate the public regarding the good Fishing Has No Boundaries®, Inc. is doing through pictures, press, and word of mouth.
To have the best angling events (not tournaments) for any person with any type of disability. In this way, all persons with disabilities are able to experience improved health, self esteem, confidence, and independence.
