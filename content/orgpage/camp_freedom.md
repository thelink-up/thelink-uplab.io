---
title: Camp Freedom
countries:
- usa
statesprovinces:
- pennsylvania
categories:
- hunting
- fishing
- hiking
- deer
- turkey
- bear
- coyote
- waterfowl
orgURL: http://www.campfreedompa.org/
---
Camp Freedom is an adventure camp for disabled Veterans & First Responders, their family members, and Gold Star families providing quality hunting, shooting sports, fishing, camping, hiking, biking, and other year round outdoor activities. These activities remove individuals from clinical settings into the healing environment of the outdoors and nature. They also promote the social well-being of the participants through peer to peer interaction, camaraderie, and outdoor adventures.  Activities are led by Camp Freedom's Staff and experienced volunteers. Camp Freedom provides services to other nonprofits who have a similar mission for a collective impact.

