---
title: Cast it Forward
countries:
- usa
statesprovinces:
- ohio
categories:
- fishing
tags:
- walleye
- fishing
orgURL: https://castitforwardfishing.com
---
Cast It Forward is a Non Profit organization passionate about sharing the love, joy, and benefits of fishing. 
Currently, we offer Lake Erie Walleye fishing excursions to kids, veterans and seniors who otherwise would not have the opportunity to do so.

To Participate
Currently, we offer Lake Erie walleye excursions in our 23’ Hewescraft Pro V fishing boat. If you are interested in fishing with us and fit our participant demographic, please contact us through this site. We will happily and promptly send you an application. Thank you for your interest!
