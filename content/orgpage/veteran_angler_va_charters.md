---
title: Veteran Angler (VA) Charters
countries:
- usa
statesprovinces:
- maine
- connecticut
categories:
- fishing
tags:
- chartered
orgURL: http://www.veterananglercharters.org
---
Fishin' With A Mission.

Veteran Angler Charters is an all-volunteer, federally recognized 501(c)(3) non-profit organization. We offer free, small group charter fishing trips to injured and recovering veterans from all conflicts and from all branches of the armed forces. Our mission is to provide our veterans with recreational rehabilitation and therapeutic support.

