---
title: Combat Marine Outdoors
countries:
- usa
statesprovinces:
- texas
categories:
- hunting
- outdoor recreation
- fishing
orgURL: https://combatmarineoutdoors.org
---
# Our Mission
To provide Dream Hunts and Outdoor Adventures for severely wounded Marines, Soldiers, Airmen, Sailors and Navy Corpsmen. These hunting and fishing excursions play a vital role in the rehabilitation of these American heroes.

# Our Goal
Our goal is to allow these heroic warriors the opportunity to have an outdoor adventure of a lifetime. Also, to make sure that they understand that their service and sacrifice to our country are deeply appreciated. It is our hope that our outdoor fellowship will aid in their vision for an exciting future.

# Our Results
Combat Marine Outdoors have witnessed hearts that have been mended, wounds that have been healed, hope for the future and lives that have been changed forever.
