---
title: America's Freedom Lodge
countries:
- usa
statesprovinces:
- ohio
categories:
- hunting
- fishing
tags:
- deer
- bass
- turkey
- pheasant
- crappie
- faith based
orgURL: http://americasfreedomlodge.org
---
America’s Freedom Lodge is a faith-based, non-partisan, non-profit 501 (c)(3) public charity. Our purpose is to show appreciation to the men and women who have served in the U.S. military, by providing a recreational program designed to accommodate those who have sacrificed much and have been disabled or paralyzed. Thank you for visiting our website. We are excited about the many things we have going on. We are growing! As more and more of you hear about our mission and join with us, we are able to reach out to our disabled veterans and their families to positively impact their lives.
