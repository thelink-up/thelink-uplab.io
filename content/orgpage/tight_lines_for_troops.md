---
title: Tight Lines for Troops
countries:
- usa
statesprovinces:
- michigan
categories:
- fishing
tags:
orgURL: http://tightlinesfortroops.com
---
Our mission is to unite organizations, businesses, sponsors and communities to help provide a free fishing tournament open to all Michigan Veterans from all eras, wartime and peacetime.  By bringing our Heroes together, we endeavor to build and foster new relationships, share their experiences and enjoy fishing in the great outdoors of Michigan.

Our all-volunteer, not-for-profit efforts will improve the quality of life for many Veterans, reinforce patriotism, educate and bring awareness that we must continue to support and honor those who have defended our country and our freedoms.
