---
title: Gathering of Mountain Eagles
countries:
- usa
statesprovinces:
- virginia
- west virginia
- pennsylvania
categories:
- hunting
- fishing
tags:
orgURL: https://mountaineagles.org
---
Mission Statement
Gathering of Mountain Eagles provides opportunities for wounded or injured American military service members and selected family members, to enjoy the therapeutic effects of adventure activities and relaxation opportunities primarily within the states of West Virginia and Virginia.

