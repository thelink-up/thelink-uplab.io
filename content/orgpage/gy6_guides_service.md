---
title: GY6 Guide Service
countries:
- usa
statesprovinces:
- wyoming
categories:
- hunting
- fishing
tags:
- guide
- outfitter
orgURL: https://www.facebook.com/groups/1223546551507607
---
GY6 GUIDES is a non profit guide service in western wyoming for disabled veterans and disabled civilians. The company is small and just starting out so as of right now we offer mule deer and elk guides for disabled veterans and disabled civilians. With proper notification and planning we will offer some antelope guides as well. We are a registered 501(c)3 non profit organization in the state of Wyoming. Since we are small and just starting out I would greatly appreciate any and all help promoting this business.
web: https://www.gy6guides.com/
insta: https://www.instagram.com/gy6_guides/
tiktok: https://www.tiktok.com/@gy6guides?lang=en
