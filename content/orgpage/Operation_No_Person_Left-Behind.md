---
title: Operation No Person Left Behind
countries:
- usa
statesprovinces:
- maryland
categories:
- hunting
- fishing
tags:
- Antelope
- Bear
- Deer (White-tail, Sika, and Mule)
- Dove
- Elk
- Turkey (Eastern, Merriam’s, Rio Grande and Osceola)
- Upland Bird (Pheasant, Chukar, and Quail)
- Waterfowl (Ducks and Geese)
- Wild Boar
orgURL: https://nplboutdoors.org
---
NPLB Outdoors was started as an adaptive hunting organization. Although it has grown to include many other types of activities, we remain well known for our phenomenal hunting opportunities across the United States. Currently, we host over 20 hunting events annually.

All our trips are provided at no cost to deserving veterans.

Our adaptive hunting program follows sustainable wildlife conservation practices and promotes safety during every hunt. We can accommodate most types of injuries and provide special adaptive equipment for veterans requiring additional help.

We have hosted veterans with upper and lower limb amputations, complete and incomplete spinal cord injuries at varying levels, vision-impairment and blindness, as well as other serious injuries. To ensure a safe and enjoyable experience for all participants, each veteran is assigned a knowledgeable mentor during every event.
