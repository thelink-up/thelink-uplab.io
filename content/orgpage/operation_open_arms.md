---
title: Operation Open Arms
countries:
- usa
statesprovinces:
- florida
categories:
- fishing
tags:
- fishing
orgURL: https://operationopenarms.org/services/fishing-and-charters/
---
Our Southwest Florida Fishing Captains have stepped up to the plate with some super offers for the service men and women who are on leave in the area. Fishing charters are limited to a single charter from only one of the contributors shown below during the 2-week period. Our captains and guides are able to offer this benefit by sharing the load among themselves. In many cases, the contributors have cancelled or rescheduled trips with paying customers so please let them know if you have to cancel your scheduled trip.
