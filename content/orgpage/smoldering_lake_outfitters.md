---
title: Smoldering Lake Outfitters
countries:
- usa
statesprovinces:
- montana
- texas
categories:
- hunting
tags:
- Outfitters
- elk
- moose
- bear
- waterfowl
- upland
- pheasant
- ducks
- geese
orgURL: https://smolderinglake.com/veterans-%26-youth
---
Smoldering Lake Outfitters have always been involved with helping terminally ill youth and wounded veterans enjoy the outdoors.  Shortly after 9/11 we began hosting groups of veterans. Seeing the therapeutic value and impact these activities had on the veterans prompted us to do more.  

Our veterans efforts grew into the establishment of The Veterans Afield Foundation 501c(3).  The mission of VAF is to assist in the reintegration and healing process for combat veterans, gold star families and first responders, through therapeutic outdoor activities. 

We are honored to grow with this program and humbled to be a part of activities that have such a positive impact of the lives of others.

