---
title: Hooks for Heroes annual fishing tournament
countries:
- usa
statesprovinces:
- connecticut
categories:
- fishing
tags:
- fishing
orgURL: https://hooksforheroesct.us
---
Hooks for Heroes is an annual fishing tournament, developed by Pat Buzzeo, member of Halloween Yacht Club and Veteran. The tournament takes place in the Long Island Sound and 100% of the donations raised are given to Operation Gift Cards to help wounded veterans and their families.
