---
title: California Waterfowl Association Veteran Hunt Program
countries:
- usa
statesprovinces:
- California
categories:
- hunting
- fishing
tags:
- deer
- pigs
- ducks
- dove
orgURL: https://calwaterfowl.org/hunt-program/veteran-hunts
---
The program promotes wellness through outdoor healing and the camaraderie of fellow veterans. Participants have enjoyed hunter education courses, fun shoots, fishing trips and hunts for waterfowl, dove and turkey from the Klamath Basin to the southern San Joaquin Valley.

CWA coordinates all hunt details and supplies food and lodging, shotgun shells, guns, jackets, gear bags, duck calls, shirts, hats and more to all participants. Hunts are free and are limited to veterans.

All of these hunts are open to any veteran currently serving or who has served in the military, naval or air service and was discharged or released under conditions other than dishonorable. A committee reviews applicants and selects veterans best suited to enjoy the hunt.

In addition to these opportunities, the Hunt Program has blinds reserved on most lottery hunt days for people who are veterans and/or mobility impaired—click here for more information.
