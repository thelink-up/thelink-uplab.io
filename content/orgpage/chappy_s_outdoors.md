---
title: Chappy's Outdoors
countries:
- usa
statesprovinces:
- north carolina
categories:
- hunting
- fishing
orgURL: http://chappysoutdoors.com
---
# Mission
The mission of Chappy's Outdoors is to conduct hunting and fishing trips for our nation's Wounded Veterans by providing training, equipment and excursions across America in order to facilitate physical, emotional and spiritual healing by sharing in God's natural creation and faithfully proclaiming the Good News of Jesus Christ through word and deed.

# What we do:
We have connected land owners, ranchers, outdoor guides and organizations, Charter/ Commercial fishermen, and Professional fishermenwithWounded Veterans to providea chance to enjoy hunting and fishing opportunities.
We have edited and recorded these tripsin order to allow our Veterans to commemorate their trip forever...in HD quality!
