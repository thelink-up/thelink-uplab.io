---
title: Mohawk Outdoors
countries:
- usa
statesprovinces:
- kentucky
categories:
- hunting
- fishing
- camping
- survival
tags:
orgURL: https://mohawk-outdoors.org
---
Mohawk Outdoors is for people who love the outdoors and want to help our combat veterans. We are an organization that assists veterans in their healing process by incorporating hunting, fishing, camping, and survival activities. Simply put, we are a small team of veterans, caregivers, and patriotic Americans that want to see the suicide rates of our fellow veterans decrease from the current 22 a day. We've personally seen how these adventures can leave a positive impact on Veteran's lives and we want to provide similar opportunities to others who may benefit. Wish us luck as we continue our journey to make a difference! All proceeds from merchandise will go to setting up outdoor adventures for our combat Veterans! Your support is greatly appreciated.
