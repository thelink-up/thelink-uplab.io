---
title: Operation Injured Soldiers
countries:
- usa
statesprovinces:
- michigan
categories:
- hunting
- fishing
tags:
- ice fishing
- deer
- pheasant
- turkey
orgURL: https://injuredsoldiers.org
---
Operation Injured Soldiers is a tax exempt, nonprofit organization founded in 2005. We provide recreational opportunities (free of charge) to wounded military veterans of all eras as a thank you for their service. The events we provide aid in the recovery from physical and emotional injuries sustained during conflict deployments.
We know that it is important to help our disabled heroes get back to doing sports and hobbies they enjoyed before being deployed. Our role is to provide events and places where disabled veterans from all wars can enjoy the camaraderie of others. 
