---
title: Black Dagger Military Hunt Club
countries:
- usa
statesprovinces:
- florida
categories:
- hunting
- fishing
- recreational
orgURL: http://www.blackdaggermhc.org
---
## Who we are
Black Dagger Military Hunt Club Inc
is a 501(c)(3) non-profit organization that provides shooting, hunting, fishing, and other outdoor opportunities for veterans andactive duty military (wounded and well)
We are located in the greater Tampa Bay Florida area but operate around the state
Many of our volunteers are retired or active duty military with a great deal of tactical and hunting experience
We co-labor with other veteran groups and nonprofits to support those who give their all for our Nation!

## Who we serve
We serve veterans from all eras without limitation to when or where they served
We work with the James A
Haley VA Hospital’s Adaptive Sports Programin Tampa Florida, the USSOCOM Care Coalition, and active duty military members on MacDill AFB
In addition, on a case-by-case basis, we will work with veterans supported by other agencies to meet a specific need that our team can satisfy
We serve veterans recovering from and learning to adjust to life with spinal cord injuries (SCI), traumatic brain injuries (TBI), loss of sight, and post-traumatic stress (PTS)
We have specialized equipment to accommodate some of the most injured military heroes thus helping our honored veterans find their “new normal”.

## Our mission
Our non-profit organizationalmission focus is to enable the veterans and active duty military personnel (both wounded and well) who have passion for hunting, shooting, fishing, and the outdoors, the ability to do so! We are a Community Partner with the James A
Haley Veterans Affairs (VA) Adaptive SportsProgram helping with rehabilitative efforts of our wounded veterans
We provide peer-to-peer outdoor activities with military members and their families, with a focus on reintegration after many years of deployments.

## Our goals
Meet the needs of veterans and expand our lessons learned to other states/agencies so they can impact veterans in their areas
Continue to develop and or adopt innovative ways to enable continued access/ability to shoot and hunt for veterans
Secure like-minded sponsors to help us maintain/expand our shooting, hunting, andoutdoorprograms.
