---
title: Touch of Nature Veteran's Adventures
countries:
- usa
statesprovinces:
- illinois
categories:
- outdoors
- hunting
tags:
- adaptive sports & fitness
- archery
- backpacking
- canoe
- kayak
- sup
- climbing
- family camps and events
- hiking
- mountain biking
- stress management
- team building
orgURL: https://ton.siu.edu/programs/veteran-adventures/
---
## WHAT IS VETERAN ADVENTURES?
Veteran Adventures is a veterans program started by SIU's [Touch of Nature Environmental Center](https://ton.siu.edu) and [SIU Veteran Services](https://veterans.siu.edu/) which aims to accomplish the following:

- Provide community building for veterans and current military personnel.
- Present challenging and adventurous opportunities to strengthen bonds between veterans and current military personnel.
- Show support and gratitude for the sacrifice that our servicemen and women have made.
- Provide veterans and current military personnel with alternatives for mental health assistance without the stigma of traditional mental health avenues.
- Foster a healthier veteran population.

Touch of Nature Environmental Center

Southern Illinois University

Mail Code 6888
Carbondale, IL 62901
618-453-1121 | F: 618-453-1188
tonec@siu.edu

## WHO CAN PARTICIPATE IN VETERAN ADVENTURES?
Veteran Adventures is open to all Post 9/11 veterans, student veterans, and members of the National Guard, the Reserves and the Registered Officers' Training Corps (ROTC).
