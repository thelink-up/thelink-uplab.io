---
title: C.A.S.T Take a Warrior Fishing
countries:
- usa
statesprovinces:
- oregon
categories:
- fishing
orgURL: http://www.castforkids.org/programs/take-a-warrior-fishing/
---
The Take a Warrior Fishing program was established in 2011 and events are designed to support military personnel and their families, specifically targeting persons assigned to Warrior Transition Commands, by creating an adaptive, community-based outdoor recreation experience through the sport of fishing
Numerous studies have shown that traumatic events not only affect the psychological structures of the self, but also the attachment and meaning that link individuals and community
This program seeks to restore those disconnects
Each event will have the capacity to accommodate up to 150 families and brings in numerous community partners.

## Goals:
- Support positive social interactions with the civilian world.
- Restore disconnect in the home by increasing family interaction.
- Encourage outdoor recreation as a therapeutic outlet.
- Empower Veterans to advocate for positive self growth and change
