---
title: Cross the Divide
countries:
- usa
statesprovinces:
- oregon
categories:
- hunting
- fishing
- recreational
orgURL: http://www.dividecamp.org/index.htm
---
## Our Mission:
Helping veterans and their families navigate the challenging terrain of life, promoting healing, hope, strength and growth by application of God’s truths in the venue of God’s creation.
