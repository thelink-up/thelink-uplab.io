---
title: Organization Name
countries:
- usa
statesprovinces:
- ohio
categories:
- hunting
- fishing
tags:
orgURL: http://www.slingandstone.org
---
Sling & Stone Outfitters is a 501(c)(3) non-profit organization that is dedicated to helping disabled veterans experience the outdoors. We help provide hunting gear, fishing tackle, and other equipment to disabled vets. Additionally we assist them in acquiring guided hunts and fishing trips.

Our veterans have sacrificed so much to allow us to live in freedom. They are taken away from their families, put in harm’s way, and some are in injured in a life changing way. Sling & Stone wants to enable them to overcome any challenges they face while pursuing their passion for the outdoors. With the help of your generous donations along with our corporate partners we are able to make a positive impact in the lives of our nations heroes. Our projects offer you a unique opportunity to directly touch the lives of disabled veterans and let them know that we still have the upmost respect and thanks for what they have done for our nation. 
