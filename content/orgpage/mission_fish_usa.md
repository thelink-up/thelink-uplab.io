---
title: Mission Fish USA
countries:
- usa
statesprovinces:
- california
categories:
- fishing
tags:
orgURL: https://www.missionfishusa.org
---
The Purpose of Mission FISH., FISHING, INTERACTING SHARING & HEALING  is to organize and plan fishing day trips and provide FISH therapy for PTSD and TBI for Veterans, Active duty service members, 1st responders and Gold Star families.
