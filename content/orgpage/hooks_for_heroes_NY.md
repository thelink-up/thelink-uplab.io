
---
title: Hooks for Heroes 
countries:
- usa
statesprovinces:
- New York
categories:
- fishing
tags:
- deep sea
orgURL: http://hooksforheroes.org
---
Hooks for Heroes is a 501(c)3 nonprofit that gives back to American Veterans & First Responders by providing them with free of cost fishing trips to help cope with the physical and mental injuries during their service. We encourage a positive outlet for these heroes and offer them a day off to relax, unwind and have a chance to catch “the big one!
