---
title: LEEK Hunting and Mountain Preserve
countries:
- usa
statesprovinces:
- pennsylvania
categories:
- hunting
- fishing
tags:
- pheasant
- deer
- turkey
- bear
- coyote
- fox
orgURL: https://www.savaw.org
---
Provide a friendly accessible camp environment and therapeutic outdoor activities like hunting and fishing, LEEK believes in encouraging our wounded heroes to focus on their abilities, without compromising their current physical limitations.

LEEK believes in providing a safe and friendly environment where wounded U.S. service members can assist each other through the healing process, both mentally and physically. We offer recreational opportunities specifically geared to each wounded veterans’ needs and abilities.
