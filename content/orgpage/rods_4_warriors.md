---
title: Rods 4 Warriors
countries:
- usa
statesprovinces:
- oklahoma
categories:
- fishing
tags:
orgURL: https://www.facebook.com/Rods4warriors
---
This is a new non-profit organization created by military veterans for veterans and their families. This organization is designed to help our military veterans find an escape from their daily struggles and enjoy a stress free environment though fishing.
How it works. If you know someone in need or you are wanting a way to get a line wet, contact Rods 4 Warriors through their Facebook group. We will set you up with a team member that can provide the opportunity.
There's so much information about returning to civilian life, VA benefits, and our entitlements after leaving the military that gets passed over or not shared. With the help of Rods 4 Warriors and our team going/went through the same experiences we can help pass this information that does get lost.
Don't know how to fish or wanting to get into a new hobby? Our team members have many years combined in the sport of fishing, we can teach you everything you need to know.
So, if you share our passion and can't get back to fishing for whatever reason, know a vet in need, or in need yourself, Rods 4 Warriors is here to provide a helping hand for military veterans and their families.
