---
title: Cape Cod Cares for the Troops
countries:
- usa
statesprovinces:
- massachusetts
categories:
- fishing
orgURL: http://www.capecod4thetroops.com
---
Cape Cod Cares for Our Troops is a non-profit 501(c)(3) all volunteer organization started in 2005 by 12 year old Dylan DeSilva. Dylan wanted to find a way to honor our troops for their service, dedication and sacrifices to our country. With the help of his family and friends he sends care packages to our troops in Iraq and Afghanistan on a weekly basis. In 2008, we added Rocky's Warriors a program to send canine care packages to our military dogs.  In 2015, we added "Talking to the Moon" a program to support care givers of our wounded warriors.
