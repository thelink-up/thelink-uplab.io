---
title: Wounded Warriors of Mississippi (not affiliated with Wounded Warrior Project)
countries:
- usa
statesprovinces:
- mississippi
categories:
- hunting
- fishing
tags:
orgURL: https://www.woundedwarriorsofms.com
---

Mississippi's Wounded Warriors are all around us. You might not always see a Veteran’s injuries since not all of them are visible. Post 9/11, we have hundreds upon hundreds that came back with physical and/or mental scars. They made the sacrifice, served our country, then came home to spouses, children, and family as a changed person. These Heroes are now fighting a war within themselves to reconnect with society. This battle they are fighting within themselves can be as difficult as the battlefield itself.

Wounded Warriors of Mississippi is a Non-Profit 501C Organization, and our mission is to help them win this battle. We help them mentally by organizing events where they can meet fellow warriors that share their daily struggles. We help them physically by assisting with daily tasks that have become too difficult to complete. We help them financially when they are struggling to pay a utility bill, get medications, or other daily needs. And we help them spiritually by praying and recognizing that without God this organization would not be what it is today and what it will grow to be.
