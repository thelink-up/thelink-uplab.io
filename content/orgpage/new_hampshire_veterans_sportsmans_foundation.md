---
title: New Hampshire Veterans Sportsmans Foundation
countries:
- usa
statesprovinces:
- new hampshire
categories:
- hunting
- fishing
tags:
orgURL: http://nhvsf.com/nhvsf/
---
The NH Veteran Sportsman Foundation assists New Hampshire Veterans with having an opportunity to hunt and fish in the state. Hunting and Fishing can be especially therapeutic activities for those with PTSD, and this foundation will use funds donated to purchase hunting and fishing licenses for veterans who need them and cannot afford them.

