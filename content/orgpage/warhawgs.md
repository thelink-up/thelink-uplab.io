---
title: WarHawgs
orgURL: http://warhawgs.org/
countries:
- usa
statesprovinces:
- alabama
categories:
- hunting
tags:
- adaptive equipment
- adaptive sports
---
The Founders, Toby Cochran (USN Vet, DAV), David Cochran (USN Vet) and Board Members, James Cochran (USN Retired), Kenny Long (USN, Army Retired, Dothan Police Retired)  and John Hodgson (Army Vet), started WarHawgs to provide an all-inclusive Adaptive Sports experience for Veterans, Active Duty and First Responders.

WarHawgs helps Disabled Veterans, Veterans, Active Duty Military and First Responders; through Outdoor Programming and Adaptive Sports, that promotes self-esteem, conflict resolution and enhances their quality of life. We serve veterans recovering from and learning to adjust to life with spinal cord injuries (SCI), traumatic brain injuries (TBI), loss of sight, and post-traumatic stress (PTS).  We have specialized equipment to accommodate some of the most injured military heroes thus helping our honored veterans find their “new normal”. Our non-profit organizational mission focus is to enable the veterans and active duty military personnel (both wounded and well) who have passion for hunting, shooting, and the outdoors, the ability to do so!  We provide outdoor activities with military members and their families, with a focus on reintegration after many years of deployments.

WarHawgs Adaptive Hunting Sports Program is unique in that we have no limitations as to who we can bring, if a military hero desires to get back in the woods, we can do it!  We have specialized adaptive equipment for veterans with spinal cord injuries of all levels and vision-impaired to include loss of sight.  We hunt deer, wild boar, turkey and a host of other wild game.  We have worked with our community partners and other agencies to build a solid program that keeps growing.
