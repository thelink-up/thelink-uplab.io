---
title: Wounded Warriors Weekend
countries:
- canada
statesprovinces:
- saskatchewan
categories:
- fishing
- golfing
- music
orgURL: https://www.woundedwarriorsweekend.org
---
We are a band of brothers and sisters who have joined together with one common goal: to show and express our heartfelt appreciation to our veterans, reservists, active duty military, and first responders who are challenged by physical and/or mental wounds sustained by active participation in recent or past military conflicts and duties.
​
Wounded Warriors Weekend is an annual event that hosts heroes who sacrificed so much for us. These men and women have sacrificed their own physical and mental well-being in order that we may live in freedom, peace, safety, and security; from the Victoria Cross and Medal of Honour Recipients who wrote the book on courage; to the brave injured men and women who come home with their lives irreparably changed but without bitterness; and to the troops on active duty who put their lives on the line for the freedom it affords all of us.

Wounded Warriors Weekend is a time for patriots, our partners, sponsors, honoured guests and friends to salute our Wounded Warriors, to thank them, to let them know they are not alone, that their courage and conviction make us profoundly grateful.
Founded in 2012 by a retired air force veteran, Wounded Warriors Weekend has evolved to include not only Veterans and Active Duty Military Personnel but also Police Officers, Trauma nurses, Correctional Officers, Firefighters, RCMP, EMTs ,First Responders and their families.
 
We bring participants from Canada as well as  the USA, the UK and Australia to experience a trusting, anxiety free, stress free weekend. We bring in resources and professionals that get to know each and every person with us that weekend.
Each of our participants goes home with new memories, a renewed sense of hope, and new friends, brothers and sisters.
