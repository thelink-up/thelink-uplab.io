---
title: Special Operations Wounded Warriors
countries:
- usa
statesprovinces:
- south carolina
categories:
- hunting
- fishing
tags:
orgURL: https://sowwcharity.com
---
SOWW (Special Operations Wounded Warriors), a 501(c)(3) Charity, was formed in August of 2012 for the distinct purpose of providing outdoor experiences and focused therapeutic retreats to a select group of both active duty and veteran U.S. Military Special Operations Forces, that have received wounds (both seen and unseen) in battle or significant training accidents, most of which that have been awarded our Country’s prestigious Purple Heart Medal.

SOWW was founded by five individuals comprised of U.S. Military Special Forces Veterans, local businessmen, and avid outdoors-man.  It was started for the intent of offering thanks and fellowship to the men and women of our Special Operation Forces who have sacrificed so greatly for our own personal freedoms and safety.

It is the belief of SOWW that we truly can make a difference in the life of a service member who has chosen to put their safety at risk while defending our freedoms and that has suffered personal injury in that endeavor. 

SOWW feels that there is not a more deserving group of individuals than our Special Operation Forces members that frequently stand in harm’s way for the protection of our freedoms, often with little or no recognition. 

These men and women pay not only a physical and mental price during their service to our Country; some have paid the ultimate price, for their fellow man. For those that return home, often with the never fading scars of their conquests, SOWW exists to demonstrate our Country’s appreciation for their efforts and contributions. It is our objective to help bridge the gap from military service to the enjoyment of the freedoms that they fought so courageously to defend.
