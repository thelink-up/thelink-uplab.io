---
title: Primitive Patriot Outdoors
countries:
- usa
statesprovinces:
- New York
categories:
- hunting
- fishing
tags:
- guide
- outfitter
orgURL: https://www.primitivepatriotoutdoors.com/the-heroes-hunt
---
Dedicated to preserving the tradition of the sportsman and women. We are a veteran owned and operated company. We are here to give advice, promote the gear we use, and help give you the tools to be successful in the field. Founded October 13, 2020, Josh had this vision to create a network to promote conservation, preservation of our public lands, and how to be ethical sportsman and women. Also, our future goal is to be able to raise money and send disabled veterans on dream hunts.

We strive to help bring the outdoor community together through our unique hosted events such as the WNY Walleye Classic, the Barcelona harbor outdoor expo, and The American  Big Buck Classic. We also run the Heroes Hunt, a program that helps veterans and first responders find peace through outdoor adventures. 

