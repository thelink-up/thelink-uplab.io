---
title: Hunting with Soldiers
countries:
- usa
statesprovinces:
- texas
categories:
- hunting
- fishing
tags:
orgURL: https://www.huntingwithsoldiers.org
---

ABOUT 
Hunting with Soldiers Inc. is approved by IRS and GuideStar as a 501(c)(3) Nonprofit
that provides hunting, fishing, and other outdoor activities to Combat Veterans.  
Hunting with Soldiers provides these opportunites to these brave individuals AT NO CHARGE TO THEM.  It is our firm belief that these men and women of uniform have paid enough in blood, sweat and tears and sacrificed enough for our freedom.  We do not require that they be an Injured Veteran we just require them to have been to combat to qualify for our Program.
 
Many injuries are not seen. Many Combat Veterans come back and suffer from PTSD. They are sent to Doctors to discuss their situations as well as given many medications. Due to the fact what they say is most often written down or recorded, many do not discuss everything. When these Men and Women are able to get back into the outdoors and sit around the camp fire, they are able to talk with like minded individuals and tell of their horrors. What they say is never repeated, thus allowing much of their nightmares to disappear with the smoke of the camp fire. We call this Outdoor Medication.
