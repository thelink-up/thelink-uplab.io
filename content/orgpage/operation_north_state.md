---
title: Operation North State
countries:
- usa
statesprovinces:
- north carolina
categories:
- fishing
tags:
orgURL: 
---
Operation North State (ONS) founded in December 2010 is a 501 c3 non-profit (ALL VOLUNTEER) military support services organization which annually offers forty-eight (48) “unique” projects for North Carolina’s active military personnel, deployed troops and veterans – especially wounded warriors / DVet. Additionally, we host nearly two-hundred 200) one-on-one recreational outings (fishing and golf) for wounded warriors / DVets throughout the year. And, we host dozens of veterans from outside North Carolina as well.
