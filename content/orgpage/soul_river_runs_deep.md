---
title: Soul River Runs Deep
countries:
- usa
statesprovinces:
- oregon
categories:
- hunting
- fishing
tags:
orgURL: https://soulriverinc.org
---
We connect inner city youth and US military veterans to the outdoors through incredible outdoor educational transformation experiences. By engaging U.S veterans as mentors for inner city youth, we believe that rich, powerful opportunities of healing authentically happen in the midst of Mother Nature. We believe that by connecting youth and veterans to our public lands, wild rivers and fresh waters, and beyond through genuine community, we will ultimately establish and inspire a new generation of outdoor leader ambassadors that will advocate for Mother Nature and conservation.
