---
title: Empowered Dream Hunts
countries:
- usa
statesprovinces:
- wisconsin
categories:
- hunting
- fishing
tags:
- bear
- fishing
- deer
orgURL: https://empowereddreamhuntsinc.org
---
Joe and Laurie Ramsey established Empowered Dream Hunts in 2007. With Joe’s love for the outdoors and hunting, Joe noticed that there was a need to help those with challenges, disabilities, or illness, so that they can have an opportunity to enjoy the outdoors as well.   As a family, the Ramsey’s have worked together in finding hunting recipients, raising funds to cover the costs, and providing a keepsake DVD to the recipient. Joe and Laurie consider the recipients and their families to be a part of the Ramsey family through these wonderful experiences.  Empowered Dream Hunts always promotes family involvement through the entire dream hunt process. The recipient’s family is encouraged to assist in fundraising efforts. They are also invited to come along and experience the hunt with their family member (recipient). Being with family makes the hunting weekend much more memorable and special.  
