---
title: Casting Vets
countries:
- usa
statesprovinces:
- south dakota
categories:
- fishing
orgURL: https://www.castingvets.org
---
Vision
 To help all veterans to recover one cast at a time.

Mission
To improve the quality of life for all VETERANS.

 Value Statement
Here at Casting Vets, we see the value in everyone. We want to be a catalyst for positive change, and since our beginnings in 2017, we’ve been driven by the same ideas we initially founded our Non-Profit Organization upon: support, empowerment, and progress.
