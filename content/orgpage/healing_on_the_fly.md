---
title: Healing on the Fly
countries:
- usa
state:
- iowa
categories:
- camping
- fishing
tags:
- resort
- vacation
orgURL: https://healingonthefly.org
---
Perpetuum Sanitatem is a 501c3 and has a unique approach to supporting those who served. Our Mission is to provide perpetual healing, both physical and emotional, to veterans of the Quad Cities. Perpetuum Sanitatem (HOTF) is a network designed to provide the veteran a multitude of opportunities to engage and to decide just how to connect with other veterans, nonprofits and volunteers supporting veterans. 'On The Fly' is a term meaning 'while in motion or progress'. HOTF allows for continuous support for the healing process. This is not a one time occurrence but designed to provide continual support. We believe this network will coordinate, fund and manage veterans support throughout the Greater Quad Cities Area. Please join us by supporting our efforts to make a measurable difference.
