---
title: Neosho Fish Hatchery Annual Rainbow for Veterans Fishing tournament
countries:
- usa
statesprovinces:
- missouri
categories:
- fishing
tags:
orgURL: https://www.fws.gov/fish-hatchery/neosho/events
---
### Who We Are

The U.S. Fish and Wildlife Service Fisheries Program has played a vital role in conserving America's fisheries since 1871, partnering with states, tribes, federal agencies, other Service programs, and private interests in efforts to conserve fish and other aquatic resources. The Fisheries Program provides a broad network of on-the-ground expertise that is unique in its geographic coverage, its array of scientific capabilities, and its ability to work strategically across political and jurisdictional boundaries.

The Neosho National Fish Hatchery is the oldest operating federal fish hatchery in the United States. Established in 1888, the hatchery is located in the Ozark Mountain Region of southwest Missouri. It is one of 70 hatcheries operated by the U.S. Fish and Wildlife Service with a mission to conserve and protect our nation’s fishery resources.

### How We Help

Over the years more than 130 different species of fish have been raised at the Neosho National Fish Hatchery. Today the hatchery rears primarily rainbow trout and pallid sturgeon. Sound science is used to efficiently produce quality rainbow trout for fishing in modified habitats contributing to healthy economies. The hatchery also enhances recreational fishing opportunities through the stocking of rainbow trout into Lake Taneycomo, providing an enormous boost to the economy. The high quality and efficient rainbow trout production at Neosho NFH is just one aspect of our fish production that creates a positive ripple effect for all Americans. Over 15,000 pallid sturgeons are reared onsite annually for release into the Missouri River as a continuous effort to offset the endangered status of this species.

