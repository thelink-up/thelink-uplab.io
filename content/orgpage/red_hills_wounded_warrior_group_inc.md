---
title: Red Hills Wounded Warrior Group, Inc
countries:
- usa
statesprovinces:
- florida
- georgia
- alabama
categories:
- hunting
- fishing
tags:
- turkey
- deer
- bass
orgURL: http://www.redhillswoundedwarrior.com/index.php
---
The Red Hills Wounded Warrior Group Inc. is a non-profit organization that was originated in 2011 by Walter Hatchett and Brian Proctor for the benefit of wounded soldiers returning from active duty overseas. The purpose of this organization is assisting in the rehabilitation of Wounded Warriors back into civilian life and returning to the work force by organizing outdoor hunting events with their fellow wounded soldiers that would otherwise not have these opportunities afforded to them. Our list of invited veterans is made up in cooperation with the National Wounded Warrior Project and consists of young men from the states of Florida, Georgia and Alabama. An additional objective is to bring awareness to all our National Heroes that have served our Country so unselfishly and given so much, but to these soldiers in particular that have returned with such severe injuries. Our hunts and gatherings have been filmed by Mossy Oak Productions for television programs to be aired to promote the National Wounded Warrior Project and organizations such as the Red Hills Wounded Warrior Group.
