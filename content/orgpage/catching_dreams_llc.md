---
title: Catching Dreams LLC
countries:
- usa
statesprovinces:
- new york
categories:
- fishing
orgURL: https://www.catchingdreamscharters.org
---
My name is Captain Ned Librock founded Catching Dreams LLC in 2015 to provide free guided fishing trips on Lake Erie, Lake Ontario, and the Niagara River.
We work through several partners, [Carly's Club from Roswell Park Cancer Institute] and [S.E.R.V. Niagara]to provide no charge charters to kids and young adults battling cancer, disabled veterans with PTSD, and those who have served our country in the military.

Angling Therapy, or Piscatorial Therapy, provides a unique and relaxing experience for the kids and veterans!
The goal is to help bring hope and healing to those we guide on their adventure by "turning wishing into fishing"!
Each charter lasts about 6 hours, features a fully loaded and professionally guided boat and provides our guests with snacks and drinks ... not to mention photos and a ball cap to commemorate the trip!

Our goal is to provide as many charters as possible, and we want to express our sincerest thanks to our corporate and individual sponsors and partners who work behind the scenes to make these trips possible.
Catching Dreams LLC is fully insured, and we work for free! Our goal is "turning wishing into fishing" for kids and veterans! Take a few minutes to look through our website and see the great results from 2015!

