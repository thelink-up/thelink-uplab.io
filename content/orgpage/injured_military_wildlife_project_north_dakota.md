---
title: Injured Military Wildlife Project of North Dakota
countries:
- usa
statesprovinces:
- north dakota
categories:
- hunting
tags:
- elk
- deer
orgURL: https://imwpnd.com
---
The IMWPND  is a group of North Dakota citizens who provide hunting and fishing opportunities to military personnel who have been injured in the line of duty.

The IMWPND provides injured service members and veterans a chance to continue hunting or fishing as they once did. The North Dakota Game and Fish Department provides 10 deer license to be used by our heroes. The IMWPND assist by providing lodging, a place to hunt, travel expenses and assistance before, during and after the hunt.
