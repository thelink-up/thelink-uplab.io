---
title: Fish on For Freedom
countries:
- usa
statesprovinces:
- michigan
categories:
- fishing
tags:
orgURL: https://www.fishonforfreedom.com
---
Fish on For Freedom is a charity, not for profit event, that will be held in our home waters of Ludington as a token of thanks from our community to the men and women that have sacrificed so much making this nation what it is today. Although a big part of the event is the activity of going out fishing on the waters of Lake Michigan, we are also designing this event to be about the camaraderie of our guests spending time together and the support our community can show towards the military veterans.
The fishing event itself will take place with a tournament style. Once fishing wraps up in the early afternoon, all the participating boats will gather outside the Ludington pier heads to proceed back into the harbor together. This will give community members a wonderful chance to line the Ludington harbor and welcome back the participants and thank them for their sacrifices for our country.
