---
title: Hooked on Heroes
countries:
- usa
statesprovinces:
- florida
categories:
- fishing
tags:
- fishing
orgURL: https://hookedonheroes.com
---
Hooked on Heroes is a non-profit organization that serves Veterans of all branches, with or without disabilities. We provide fishing trips for veterans at events. Our company supplies all the fishing needs necessary, including their fishing license for the day, so these men and women can enjoy a trip on the water, so they are not responsible for paying a dime. We provide various fishing items such as, fishing rods, reels, hooks, fishing line, popping corks, artificial bait, and other items. We supply such items and lunch for the day. Not only do we provide a day of fishing for our veterans but we pride ourselves in giving back to the people who ensure our freedom.
