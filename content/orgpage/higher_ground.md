---
title: Higher Ground
countries:
- usa
statesprovinces:
- Idaho
- Nationwide
categories:
- flyfishing
tags:
- retreat
- flyfishing
- salmon
- trout
orgURL: https://highergroundusa.org/programs/veteran-programs/
---
We use recreation, therapy, and continuing support to give people of all abilities a better life. Together we bridge the gap between disability and belonging
There is no other program in the country that offers the whole-life healing that we do at Higher Ground. We are the only program that invites the spouse or other supporter to be part of the journey. Together, the pair forms a new unit with other veterans and supporters in their program. They take on new challenges. They talk.
They listen. They feel heard. They feel hope.
The real healing begins when they head home. Higher Ground guarantees the veteran can stay active in recreation by providing the equipment and finances needed. We also commit to three years of follow-up support. We check in, we encourage, and we help them remember that they are not in this alone.
