---
title: Fishing with Vets
countries:
- usa
statesprovinces:
- minnesota
categories:
- fishing
tags:
orgURL: https://www.fishingwithvets.com
---
Fishing with Vets is a non-profit organization with a mission to provide veterans, both active and retired, with an opportunity to fish some of the mid-west’s premier fishing destinations.  The veterans are paired up with guides who are some of Midwest's best fishermen/women.  The main goal of our events is to create life-long memories and friendships for our veterans.  To Those Who Served : To Those Still Serving
