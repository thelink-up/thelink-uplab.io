---
title: Heroes on the Water
countries:
- usa
statesprovinces:
- texas
categories:
- fishing
tags:
- kayaking
orgURL: https://heroesonthewater.org
---
Our United States military veterans and personnel make the ultimate sacrifice for us daily. Heroes on the Water understands that this role can come with stress and trauma and we are dedicated to supporting the overall physical and mental healing process. Our goal is to build a community where our veterans and active-duty participants feel comfortable and valued.  

Our chapter events are designed to create a welcoming and pressure-free environment and are proven to help reduce symptoms of post-traumatic stress and traumatic brain injury. They provide a chance for participants to reconnect with themselves and others. 

Events are open to family members of veterans and first responders at no cost because we believe that the focus should be on the whole person, which includes family as well.
