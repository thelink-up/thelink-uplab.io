---
title: Warfighter Outfitters
countries:
- usa
statesprovinces:
- oregon
categories:
- hunting
- fishing
tags:
- deer
- bear
- turkey
- salmon
- sturgeon
orgURL: https://warfighteroutfitters.org
---
We offer hunting and fishing trips along with opportunities for team building and comradery through service projects.

Our fishing trips offer everything from exciting, fast paced jet excursions up the rivers for steelhead to calm lake trips for Kokanee to Drift boat floats through wild and scenic rivers for Fish on the fly rod.

Our Hunt trips are the top tier and most coveted of our events featuring fully guided and supported Big game, Bird, Varmint and Predator animal seasons in the Pacific Northwest.
