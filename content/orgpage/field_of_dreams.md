---
title: Field of Dreams
countries:
- usa
statesprovinces:
- California
categories:
- hunting
- fishing
tags:
- elk
- waterfowl
- pigs
- trout
- deer
- salmon
- striped bass
- turkey
- bear
- antelope
orgURL: https://www.fieldofdreamsinc.org
---
Field of Dreams is made up of an extremely dedicated group of men and women who love the outdoors, and want to share their wealth of knowledge and means of opportunity with others. We're incredibly grateful to be able to give back to members of the military, their families, and special needs kids in this way. 

It started simply, with a fishing trip planned for two special needs kids. Each year we received more requests to attend, and now our annual fishing day caters to over thirty special needs kids and their families. Since that first year we've widened our scope of invitees to include men and women who are enlisted in the military. We take these very deserving individuals on outdoor adventures ranging from bear hunts in Manitoba, Canada, big game hunting in Africa, and everything in between. Every year, thanks to the tireless efforts of our dedicated staff and amazing sponsors and donors, we're able to expand the number of excursions we offer and veterans we help. 

Even when a physical disability of a child or veteran limits mobility, we help to find ways to overcome these challenges and provide opportunities for adventures in the field. We are incredibly proud of the impact these experiences have in the lives of everyone involved, and we wouldn't be able to do any of this without the support of our community. Check out our sponsors page to learn more about our wonderful support team, visit our Donations page to learn how you can help us make the Field of Dreams experience a reality for more very deserving individuals
