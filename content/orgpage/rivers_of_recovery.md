---
title: Rivers of Recovery
countries:
- usa
statesprovinces:
- wyoming
categories:
- fishing
tags:
orgURL: http://www.riversofrecovery.org
---
Rivers of Recovery is dedicated to providing rehabilitation to physically and psychologically injured combat veterans through innovative, outdoor-based therapies and pioneering research.  We strive to provide our participants with therapeutic programs which result in measurable and sustainable improvement.

Our programs are designed to re-enable and re-energize participants and provide the support and self-confidence necessary to maximize long-term recovery
